package com.ATHLAS.operator_terminal.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.util.AttributeSet;

/**
 * Second variant of a leg display. Styling is similar to the original but more refined, based on
 * feedback of a helicopter pilot: Sidelong bars for leg state visualization, added earth height text
 * and tightened layout to fit.
 *
 * @author Flurin Schwerzmann
 */

public class LegStatusViewVariant2 extends LegStatusView {

    /**
     * View height percentage
     */
    float footBaseHeight = 5f, legHeightTextHeight = 14;

    /**
     * Sideward margin of foot line in screen dpi.
     */
    float footLineMargin = 17;

    /**
     * Locally different earth min height and leg min/max height boundaries due to changed layout. <br>
     * <ul>
     *     <li>Leg max: 68%</li>
     *     <li>Leg min: 19%</li>
     *     <li>Earth lower bound: 86%</li>
     * </ul>
     */
    public LegStatusViewVariant2(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);

        this.LEG_MAX_LENGTH_HEIGHT_PERCENTAGE = 68;
        this.LEG_MIN_LENGTH_HEIGHT_PERCENTAGE = 19;
        this.EARTH_LOWER_BOUND_PERCENTAGE = 86;
    }

    /**
     * Slightly adjusted drawing of sky and earth due to changed layout.
     *
     * @param canvas drawing area
     */
    @Override
    void drawBackground(Canvas canvas) {
        canvas.drawRect(0, 0, width, height, blackBackgroundPaint); //black background
        canvas.drawRect(0, hp2p(legHeightTextHeight, 0), width, hp2p(earthViewPercentage, 0), skyPaint); //blue sky
        canvas.drawRect(0, hp2p(earthViewPercentage, 0), width, hp2p(EARTH_LOWER_BOUND_PERCENTAGE, 0), earthPaint); //brown earth
    }

    /**
     * {@code legStatePaint} has paint-style {@code FILL} to be used for sidelong leg state
     * visualization bars.
     */
    @Override
    void initPaint() {
        super.initPaint();
        legStatePaint.setStyle(Paint.Style.FILL);
    }

    /**
     * Slimmer foot base to fit changed layout.
     *
     * @param canvas drawing area
     */
    @Override
    void drawFootBase(Canvas canvas) {
        Path footBase = new Path();

        //left
        PointF A = new PointF(wp2p(0, 0), hp2p(legHeightTextHeight, 0)),
                B = new PointF(wp2p(25, 0), A.y),
                C = new PointF(wp2p(15, 0), hp2p(legHeightTextHeight + footBaseHeight, 0)),
                D = new PointF(A.x, C.y);
        footBase.moveTo(A.x, A.y);
        footBase.lineTo(B.x, B.y);
        footBase.lineTo(C.x, C.y);
        footBase.lineTo(D.x, D.y);
        footBase.lineTo(A.x, A.y);
        canvas.drawPath(footBase, footBasePaint);

        //right
        A.x = wp2p(100, 0);
        B.x = wp2p(75, 0);
        C.x = wp2p(85, 0);
        D.x = A.x;
        footBase.moveTo(A.x, A.y);
        footBase.lineTo(B.x, B.y);
        footBase.lineTo(C.x, C.y);
        footBase.lineTo(D.x, D.y);
        footBase.lineTo(A.x, A.y);
        canvas.drawPath(footBase, footBasePaint);
    }

    /**
     * Slimmer foot fitting the slimmer foot base.
     *
     * @param canvas drawing area
     */
    @Override
    void drawLeg(Canvas canvas) {
        //leg
        canvas.drawRect(wp2p(40, 0), hp2p(legHeightTextHeight, 0), wp2p(60, 0), hp2p(legViewPercentage, 0), legPaint);

        //draw the foot
        Path foot = new Path();
        PointF A = new PointF(wp2p(25, 0), hp2p(legViewPercentage - footBaseHeight, 0)),
                B = new PointF(wp2p(75, 0), A.y),
                C = new PointF(wp2p(85, 1f), hp2p(legViewPercentage, 0)),
                D = new PointF(wp2p(15, -1.5f), C.y);
        foot.moveTo(A.x, A.y);
        foot.lineTo(B.x, B.y);
        foot.lineTo(C.x, C.y);
        foot.lineTo(D.x, D.y);
        foot.lineTo(A.x, A.y);
        canvas.drawPath(foot, footPaint);

        //foot line
        canvas.drawLine(wp2p(0, footLineMargin), hp2p(legViewPercentage, 0),
                wp2p(100, -footLineMargin), hp2p(legViewPercentage, 0),
                footPaint);
    }

    /**
     * Added earth height text, change from leg state frame to sidelong bars and added
     * white separator lines at the bottom of the leg height text as well as on top
     * of the earth height text.
     *
     * @param canvas drawing area
     */
    @Override
    void drawFrame(Canvas canvas) {
        //leg height text
        canvas.drawText(String.valueOf((int) legHeightPercentage) + "%", wp2p(50, 0), hp2p(7, 0) - (textPaint.descent() + textPaint.ascent())/2, textPaint);

        //earth height text
        canvas.drawText(String.valueOf((int) earthHeightPercentage) + "%", wp2p(50, 0), hp2p(93, 0) - (textPaint.descent() + textPaint.ascent())/2, textPaint);

        //View borders rectangle
        canvas.drawRect(0, 0, width, height, purpleLinePaint);

        //leg state sidelong bars
        canvas.drawRect(broaderStrokeWidth/2f, hp2p(legHeightTextHeight + footBaseHeight, 0),
                wp2p(0, footLineMargin), hp2p(LEG_MAX_LENGTH_HEIGHT_PERCENTAGE, 0), legStatePaint); //left
        canvas.drawRect(width - broaderStrokeWidth/2f, hp2p(legHeightTextHeight + footBaseHeight, 0),
                wp2p(100, -footLineMargin), hp2p(LEG_MAX_LENGTH_HEIGHT_PERCENTAGE, 0), legStatePaint); //right

        //max leg travel line
        float horizonLineMargin = 3,
                legMaxLength = hp2p(LEG_MAX_LENGTH_HEIGHT_PERCENTAGE, 0);
        canvas.drawLine(wp2p(0, horizonLineMargin), legMaxLength,
                wp2p(100, -horizonLineMargin), legMaxLength,
                maxLegLengthPaint);

        //white leg height text bottom separator line
        float footBaseTop = hp2p(LEG_MIN_LENGTH_HEIGHT_PERCENTAGE - footBaseHeight, 0);
        canvas.drawLine(wp2p(0, horizonLineMargin), footBaseTop,
                wp2p(100, -horizonLineMargin), footBaseTop,
                fineWhiteLinePaint);

        //white earth height text top separator line
        float earthLowerBound = hp2p(EARTH_LOWER_BOUND_PERCENTAGE, 0);
        canvas.drawLine(wp2p(0, horizonLineMargin), earthLowerBound,
                wp2p(100, -horizonLineMargin), earthLowerBound,
                fineWhiteLinePaint);
    }
}
