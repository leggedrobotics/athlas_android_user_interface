/******************************************************************************
 * Copyright (c) 2016 All Rights Reserved, http://www.rsl.ethz.ch             *
 * ETH Zurich                                                                 *
 * Robotic Systems Lab                                                        *
 * Leonhardstrasse 21, LEE J201                                               *
 * 8092 Zurich                                                                *
 * Switzerland                                                                *
 * ---                                                                        *
 * Contributors:                                                              *
 * - Samuel Bachmann (samuel.bachmann@gmail.com)                              *
 ******************************************************************************/

package com.ATHLAS.operator_terminal.tools;

import android.support.design.widget.Snackbar;

public class SnackbarMessage {
  private String message_ = "";
  private int duration_ = Snackbar.LENGTH_LONG;

  public SnackbarMessage(String message) {
    message_ = message;
  }

  public SnackbarMessage(String message, int duration) {
    message_ = message;
    duration_ = duration;
  }

  public String getMessage() {
    return message_;
  }

  public int getDuration() {
    return duration_;
  }
}
