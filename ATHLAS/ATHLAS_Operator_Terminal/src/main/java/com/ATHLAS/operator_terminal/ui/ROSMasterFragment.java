/******************************************************************************
 * Copyright (c) 2016 All Rights Reserved, http://www.rsl.ethz.ch             *
 * ETH Zurich                                                                 *
 * Robotic Systems Lab                                                        *
 * Leonhardstrasse 21, LEE J201                                               *
 * 8092 Zurich                                                                *
 * Switzerland                                                                *
 * ---                                                                        *
 * Contributors:                                                              *
 * - Samuel Bachmann (samuel.bachmann@gmail.com)                              *
 * - Flurin Schwerzmann (f.schwerzmann@gmail.com)                                                                         *
 ******************************************************************************/

package com.ATHLAS.operator_terminal.ui;

import android.app.Fragment;
import java.net.URI;
import java.net.URISyntaxException;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ATHLAS.operator_terminal.R;

import org.ros.address.InetAddressFactory;
import org.ros.exception.RosRuntimeException;
import org.ros.internal.node.client.MasterClient;
import org.ros.internal.node.xmlrpc.XmlRpcTimeoutException;
import org.ros.namespace.GraphName;
import org.ros.node.DefaultNodeMainExecutor;
import org.ros.node.NodeConfiguration;
import org.ros.node.NodeMainExecutor;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Fragment providing simple ROS connection utilities.
 *
 * @author Flurin Schwerzmann
 * @author Samuel Bachmann
 */
public class ROSMasterFragment extends Fragment {

  protected static final String TAG = "ROSMasterFragment";

  //Butterknife stuff
  @BindView(R.id.editTextMasterUri) EditText editTextMasterUri_;
  @BindView(R.id.textViewRosMasterStatus) TextView ROSMasterStatusTv;
  @BindView(R.id.buttonRosConnect) Button btnConnect;
  @BindView(R.id.buttonRosDisconnect) Button btnROSDisconnect;
  @BindView(R.id.buttonSettings) Button buttonSettings;
  @BindView(R.id.ros_master_linlay) LinearLayout mainLinLay;
  @BindString(R.string.ros_master) String fragmentTitle_;
  @BindString(R.string.no_toolbar_available) String noToolbarAvailable_;
  @BindString(R.string.KEY_ROS_MASTER_IP) String keyRosMasterIP_;
  @BindString(R.string.KEY_ROS_MASTER_PORT) String keyRosMasterPort_;
  @BindDrawable(R.drawable.ic_wifi_tethering_white_24dp) Drawable drawableWifiOn_;
  @BindDrawable(R.drawable.ic_portable_wifi_off_white_24dp) Drawable drawableWifiOff_;
  Unbinder unbinder_;

  private MainApplication mainApplication;
  private NodeMainExecutor nodeMainExecutor_;
  private NodeConfiguration nodeConfiguration_;
  private MainActivity mainActivity;


  /**
   * Inflates layout and nitializes UI elements.
   */
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);

    View rootView = inflater.inflate(R.layout.fragment_ros_master, container, false);

    unbinder_ = ButterKnife.bind(this, rootView);

    try {
      ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(fragmentTitle_);
    } catch (NullPointerException e) {
      Log.e(TAG, noToolbarAvailable_);
    }

    mainActivity = (MainActivity) getActivity();
    mainApplication = (MainApplication) mainActivity.getApplicationContext();


    editTextMasterUri_.setEnabled(false);

    return rootView;
  }

  /**
   * Updates the UI elements, including the connection info {@link #ROSMasterStatusTv text view}. <br>
   * <b>Note:</b> Autoconnect may be enabled here.
   */
  @Override
  public void onResume() {
    super.onResume();

    SharedPreferences sharedPreferences =
        PreferenceManager.getDefaultSharedPreferences(getActivity());
    String valueIp = sharedPreferences.getString(keyRosMasterIP_, "");
    String valuePort = sharedPreferences.getString(keyRosMasterPort_, "");

    Log.i(TAG, "Shared preferences, ip: " + valueIp);
    Log.i(TAG, "Shared preferences, port: " + valuePort);

    String uri = "http://" + valueIp + ":" + valuePort + "/";
    editTextMasterUri_.setText(uri);

    if (mainApplication.isConnected()) {
      ROSMasterStatusTv.setText(R.string.connected);
    } else {
      ROSMasterStatusTv.setText(R.string.disconnected);
    }
    Log.i(TAG, "onResume, ROS status TV updated");

//    connect(); //Autoconnect here!
  }

  /**
   * Connect with ROS, action for the {@link #btnConnect}. Success as well as exceptions are
   * displayed using {@link Snackbar Snackbars}.
   */
  @OnClick(R.id.buttonRosConnect)
  public void connect() {
    btnConnect.setEnabled(false);
    btnROSDisconnect.setEnabled(false);
    final String uri = editTextMasterUri_.getText().toString();

    // Make sure the URI can be parsed correctly and that the master is reachable.
    new AsyncTask<Void, Void, Boolean>() {
      /**
       * Tries to connect with ROS and shows a <i>INIT ROS CNCT</i> {@link Snackbar}. A successful or
       * erroneous connection is announced using a Snackbar, too.
       *
       * @return true: ROS connected, false: otherwise
         */
      @Override
      protected Boolean doInBackground(Void... params) {
        String status;
        try {
          String message = "INIT ROS CNCT";
          Log.d(TAG, message);
          Snackbar.make(mainActivity.navigationView, message, Snackbar.LENGTH_SHORT).show();

          MasterClient masterClient = new MasterClient(new URI(uri));
          masterClient.getUri(GraphName.of("android/master_chooser_activity"));

          status = "ROS CNCTD";
          Log.d(TAG, status);
          Snackbar.make(mainActivity.navigationView, status, Snackbar.LENGTH_SHORT).show();

          mainActivity.operatorTerminalNode = null;

          return true; //ROS connected
        } catch (URISyntaxException e) {
          status = "WRONG URI";
        } catch (XmlRpcTimeoutException e) {
          status = "ROS NO CNCTN";
        } catch (RosRuntimeException e) {
          status = "ROS: RosRuntimeException"  + e.toString();
        } catch (NullPointerException e) {
          status = "ROS: NullPointerException" + e.toString();
        } catch (RuntimeException e) {
          status = "ROS: RuntimeException"  + e.toString();
        }
        Log.e(TAG, status);
        if (mainActivity.navigationView != null) Snackbar.make(mainActivity.navigationView, status, Snackbar.LENGTH_SHORT).show();

        return false; //ROS not connected
      }

      /**
       * Handles ROS connection attempt results and reenable the disconnect button.<br>
       * If successful: Set up {@link MainApplication} ROS framework for later access, update the
       *                info text view with <i>connected</i> and disable the connect button.
       * If unsuccessful: Display a {@link Snackbar} <i>ROS N/A</i>.
       *
       * @param result true: ROS connected, false: otherwise
       */
      @Override
      protected void onPostExecute(Boolean result) {
        if (result) { //ROS connected
          mainApplication = (MainApplication) getActivity().getApplicationContext();
          nodeMainExecutor_ = mainApplication.getNodeMainExecutor();
          nodeConfiguration_ = mainApplication.getNodeConfiguration();

          nodeConfiguration_ = NodeConfiguration.newPublic(InetAddressFactory.newNonLoopback()
              .getHostAddress());
          URI uri = URI.create(editTextMasterUri_.getText().toString());
          nodeConfiguration_.setMasterUri(uri);
          nodeMainExecutor_ = DefaultNodeMainExecutor.newDefault();

          mainApplication.setNodeMainExecutor(nodeMainExecutor_);
          mainApplication.setNodeConfiguration(nodeConfiguration_);
          mainApplication.setRosMasterUri(editTextMasterUri_.getText().toString());
          mainApplication.setConnected(true);

          ROSMasterStatusTv.setText(R.string.connected);

          if(btnConnect != null) btnConnect.setEnabled(false);

        } else { //ROS not connected
          Snackbar.make(mainActivity.navigationView, "ROS N/A",
              Snackbar.LENGTH_LONG).show();

          if(btnConnect != null) btnConnect.setEnabled(true);
        }
        if(btnROSDisconnect != null) btnROSDisconnect.setEnabled(true);
      }
    }.execute();
  }

  /**
   * Shuts down ROS connection, action for the {@link #btnROSDisconnect}.
   */
  @OnClick(R.id.buttonRosDisconnect)
  public void disconnect() {
    mainApplication = (MainApplication) getActivity().getApplicationContext();
    nodeMainExecutor_ = mainApplication.getNodeMainExecutor();
    if (nodeMainExecutor_ != null) {
      nodeMainExecutor_.shutdown();
    }

    nodeMainExecutor_ = null;
    nodeConfiguration_ = null;
    mainApplication.setNodeMainExecutor(null);
    mainApplication.setNodeConfiguration(null);

    mainApplication.setConnected(false);
    ROSMasterStatusTv.setText(R.string.disconnected);
    Snackbar.make(mainActivity.navigationView, "ROS DISC", Snackbar.LENGTH_SHORT).show();

    mainActivity.operatorTerminalNode = null;
    btnConnect.setEnabled(true);
    btnROSDisconnect.setEnabled(false);
  }

  /***
   * Opens {@link UserSettingActivity}.
   */
  @OnClick(R.id.buttonSettings)
  public void settings(){
    Intent intent = new Intent(getActivity(), UserSettingActivity.class);
    startActivity(intent);
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    Log.d(TAG, "onDestroyView()");
    unbinder_.unbind();
  }
}
