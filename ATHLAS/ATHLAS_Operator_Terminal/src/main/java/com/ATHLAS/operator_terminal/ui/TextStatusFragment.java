package com.ATHLAS.operator_terminal.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.ATHLAS.operator_terminal.R;
import com.ATHLAS.operator_terminal.ros.nodes.OperatorTerminalNode;

import java.util.ArrayList;

/**
 * Fragment providing touch-to-dismiss status and error messages displays.
 *
 * @author Flurin Schwerzmann
 * @author Luca Vandeventer
 */
public class TextStatusFragment extends Fragment {

    View rootView;
    MainActivity mainActivity;

    /**
     * Status/Error messages for real mode and ROS connection.
     */
    @SuppressWarnings("FieldCanBeLocal")
    private final String REAL_M_NOT_ACTIV = "REAL M NOT ACTIV",
                         REAL_M_ACTIV = "REAL M ACTIV",
                         ROS_CNCTD = "ROS CNCTD",
                         ROS_NO_CNCTN = "ROS NO CNCTN";

    /**
     * Display for multiple status/error messages.
     */
    ListView errorLv, statusLv;

    /**
     * Stores status/error message strings.
     */
    private ArrayList<String> errorList, statusList;

    /**
     * Binds the status/error message {@link #errorList strings} to their {@link #errorLv displays}.
     */
    ArrayAdapter<String> errorAdapter, statusAdapter;

    private final String TAG = "TextStatusFragment";

    /**
     * Prepares the {@link #errorList list objects}.
     * @see #initLists()
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initLists();
    }

    /**
     * Initializes the {@link #errorLv list views}.
     *
     * @see #setUpListViews(View)
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        rootView = inflater.inflate(R.layout.fragment_text_status_display, container, false);
        mainActivity = (MainActivity) getActivity();

        setUpListViews(rootView);

        return rootView;
    }

    /**
     * Updates ROS connection status/error using {@link #pushMessageROSConnection()}.
     */
    @Override
    public void onResume() {
        super.onResume();

        pushMessageROSConnection();
    }

    /**
     * Adds an error message to the {@link #errorLv}. Checks for message duplicates and null
     * {@link #errorList} are performed.
     *
     * @param error The error message to be displayed
     */
    public void pushError(String error){
        initLists();

        if(!errorList.contains(error)) {
            errorList.add(error);
            mainActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    errorAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    /**
     * Adds a status message to the {@link #statusLv}. Checks for message duplicates and null
     * {@link #statusList} are performed.
     *
     * @param status The status message to be displayed.
     */
    public void pushStatus(String status){
        initLists();

        if(!statusList.contains(status)) {
            statusList.add(status);
            mainActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    statusAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    /**
     * Called by {@link OperatorTerminalNode} after starting node has completed.
     * Displays node status on the {@link TextStatusFragment text status fragment}.
     * @param isNodeReady true: node startup was entirely successful, false: otherwise
     */
    public void onNodeStarted(boolean isNodeReady){
        pushMessageROSConnection();
        if (!isNodeReady) pushError("ERR STARTG NODE");
    }

    /**
     * Checks ROS connection using {@link MainApplication#isConnected()} and either pushes a status
     * <i>ROS CNCTD</i> or an error <i>ROS NO CNCTN</i>. Conflicting messages are avoided.
     */
    public void pushMessageROSConnection(){
        initLists();

        if(mainActivity.mainApplication.isConnected()) {
            if(errorList.contains(ROS_NO_CNCTN)) errorList.remove(ROS_NO_CNCTN);
            pushStatus(ROS_CNCTD);
        } else {
            if(statusList.contains(ROS_CNCTD)) statusList.remove(ROS_CNCTD);
            pushError(ROS_NO_CNCTN);
        }
    }

    /**
     * Displays a <i>REAL M (NOT) ACTIV</i> status. Conflicting status messages are avoided.
     */
    public void pushStatusRealMode(boolean isReal){
        initLists();

        if(isReal) {
            if(statusList.contains(REAL_M_NOT_ACTIV)) statusList.remove(REAL_M_NOT_ACTIV);
            pushStatus(REAL_M_ACTIV);
        } else {
            if(statusList.contains(REAL_M_ACTIV)) statusList.remove(REAL_M_ACTIV);
            pushStatus(REAL_M_NOT_ACTIV);
        }
    }

    /**
     * Initialize {@link #errorList} and {@link #statusList} if necessary.
     */
    private void initLists(){
        if(errorList == null) errorList = new ArrayList<>();
        if(statusList == null) statusList = new ArrayList<>();
    }

    /**
     * Initializes {@link #errorLv} and {@link #statusLv} using {@code rootView} and the corresponding
     * {@link #errorAdapter adapters} and {@link #errorList lists}. Error messages are printed red,
     * status messages white. Touch-to-dismiss funcionality is set up.
     * @param rootView The rootView, where {@link #errorLv} and {@link #statusLv} can be found.
     */
    private void setUpListViews(View rootView){
        errorLv = (ListView) rootView.findViewById(R.id.status_error_lv);
        errorAdapter = new ArrayAdapter<String>(getActivity(), R.layout.list_element_error, errorList);
        errorLv.setAdapter(errorAdapter);
        errorLv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                errorList.remove(position);
                errorAdapter.notifyDataSetChanged();
            }
        });

        statusLv = (ListView) rootView.findViewById(R.id.status_info_lv);
        statusAdapter = new ArrayAdapter<String>(getActivity(), R.layout.list_element_status, statusList);
        statusLv.setAdapter(statusAdapter);
        statusLv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                statusList.remove(position);
                statusAdapter.notifyDataSetChanged();
            }
        });
    }
}
