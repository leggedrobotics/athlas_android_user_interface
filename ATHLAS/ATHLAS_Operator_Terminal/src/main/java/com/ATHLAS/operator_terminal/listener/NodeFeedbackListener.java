/******************************************************************************
 * Copyright (c) 2016 All Rights Reserved, http://www.rsl.ethz.ch             *
 * ETH Zurich                                                                 *
 * Robotic Systems Lab                                                        *
 * Leonhardstrasse 21, LEE J201                                               *
 * 8092 Zurich                                                                *
 * Switzerland                                                                *
 * ---                                                                        *
 * Contributors:                                                              *
 * - Samuel Bachmann (samuel.bachmann@gmail.com)                              *
 * -                                                                          *
 ******************************************************************************/

package com.ATHLAS.operator_terminal.listener;

import com.ATHLAS.operator_terminal.tools.SnackbarMessage;

public interface NodeFeedbackListener {
  void onSnackbar(SnackbarMessage snackbarMessage);
}
