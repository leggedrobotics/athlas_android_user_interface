package com.ATHLAS.operator_terminal.ui;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;

import com.ATHLAS.operator_terminal.R;

/**
 * Display illustrating the state of an {@link AthlasLeg}:
 * <ul>
 *     <li>{@link AthlasLeg#legHeight leg height}</li>
 *     <li>{@link AthlasLeg#earthHeight earth height}</li>
 *     <li>{@link AthlasLeg#brakeState brake state}</li>
 *     <li>{@code ground contact} (currently no sensor implemented)</li>
 * </ul>
 *
 * @author Flurin Schwerzmann
 */

public class LegStatusView extends View {
//    final String TAG = "LegStatusView";

    /**
     * Color and fill/stroke drawing style.
     */
    Paint fineWhiteLinePaint,
            legStatePaint,
            purpleLinePaint,
            earthPaint,
            maxLegLengthPaint,
            broaderGreyLinePaint,
            textPaint,
            footBasePaint,
            skyPaint,
            legPaint,
            footPaint,
            blackBackgroundPaint;

    /**
     * Local view dimension variable (pixels), set during {@link #onSizeChanged(int, int, int, int)}.
     */
    int width, height;

    /**
     * Dimension in pixels.
     */
    float frameStrokeWidth,
            broaderStrokeWidth,
            whiteLineWidth,
            textSize;

    /**
     * Leg position drawing boundaries as of view height percentage.
     */
    float LEG_MIN_LENGTH_HEIGHT_PERCENTAGE = 27, LEG_MAX_LENGTH_HEIGHT_PERCENTAGE = 80;

    /**
     * Earth drawing boundary as of view height percentage.
     */
    float EARTH_LOWER_BOUND_PERCENTAGE = 100;

    /**
     * Leg/earth position as of view height percentage.
     */
    float legViewPercentage = 0,
            earthViewPercentage = LEG_MAX_LENGTH_HEIGHT_PERCENTAGE + 5;

    /**
     * Leg/earth position as of leg/earth height range percentage.
     */
    float legHeightPercentage = 0,
            earthHeightPercentage = 100;

    boolean hasGroundContact = false;

    /**
     * @see AthlasLeg.LegState
     */
    AthlasLeg.LegState legState = AthlasLeg.LegState.Error;

    Resources res;
    DisplayMetrics displayMetrics;


    public LegStatusView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);

        loadResources();
        initPaint();

        //default values
        setEarthHeightPercentage(100.0f);
        setLegHeightPercentage(0f);
    }

    /**
     * Draws {@link #drawBackground(Canvas) background}, {@link #drawFootBase(Canvas) foot base},
     * {@link #drawLeg(Canvas) leg} and {@link #drawFrame(Canvas) frame}.
     *
     * @param canvas drawing area
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        drawBackground(canvas);
        drawFootBase(canvas);
        drawLeg(canvas);
        drawFrame(canvas);

    }

    /**
     * Loads dimensions from XML resources.
     */
    private void loadResources(){
        res = getResources();
        whiteLineWidth = res.getDimension(R.dimen.lsd_fine_line);
        frameStrokeWidth = res.getDimension(R.dimen.lsd_frame_stroke_width_dpi);
        broaderStrokeWidth = res.getDimension(R.dimen.lsd_middle_broad_line);
        textSize = res.getDimension(R.dimen.lsd_text_size);
    }

    /**
     * Set up dye and styling.
     */
    @SuppressWarnings("deprecation")
    void initPaint(){
        fineWhiteLinePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        fineWhiteLinePaint.setStrokeWidth(whiteLineWidth);
        fineWhiteLinePaint.setStyle(Paint.Style.STROKE);
        fineWhiteLinePaint.setColor(res.getColor(R.color.colorWhite));

        maxLegLengthPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        maxLegLengthPaint.setStrokeWidth(broaderStrokeWidth);
        maxLegLengthPaint.setStyle(Paint.Style.STROKE);
        maxLegLengthPaint.setColor(res.getColor(R.color.colorWhite));

        broaderGreyLinePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        broaderGreyLinePaint.setStrokeWidth(broaderStrokeWidth);
        broaderGreyLinePaint.setStyle(Paint.Style.STROKE);
        broaderGreyLinePaint.setColor(res.getColor(R.color.colorGray));

        legStatePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        legStatePaint.setStrokeWidth(frameStrokeWidth);
        legStatePaint.setStyle(Paint.Style.STROKE);
        legStatePaint.setColor(res.getColor(R.color.colorWarning));

        //border Paint
        purpleLinePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        purpleLinePaint.setStrokeWidth(broaderStrokeWidth);
        purpleLinePaint.setStyle(Paint.Style.STROKE);
        purpleLinePaint.setColor(res.getColor(android.R.color.holo_purple));

        earthPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        earthPaint.setStyle(Paint.Style.FILL);
        earthPaint.setColor(res.getColor(R.color.color_earth));

        //background Paint
        skyPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        skyPaint.setStyle(Paint.Style.FILL);
        skyPaint.setColor(res.getColor(R.color.color_athlas_blue));

        textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        textPaint.setTextSize(textSize);
        textPaint.setTextAlign(Paint.Align.CENTER);
        textPaint.setColor(res.getColor(R.color.colorWhite));

        footBasePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        footBasePaint.setStyle(Paint.Style.FILL);
        footBasePaint.setColor(res.getColor(R.color.colorGray));

        legPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        legPaint.setStyle(Paint.Style.FILL);
        legPaint.setColor(res.getColor(R.color.colorSwing));

        footPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        footPaint.setStyle(Paint.Style.FILL);
        footPaint.setColor(res.getColor(R.color.colorSwing));
        footPaint.setStrokeWidth(broaderStrokeWidth);

        blackBackgroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        blackBackgroundPaint.setStyle(Paint.Style.FILL);
        blackBackgroundPaint.setColor(res.getColor(R.color.colorBlack));
    }

    /**
     * Draws
     * <ul>
     *     <li>pitch black border to border</li>
     *     <li>blue sky from left to right and
     *     below the percentage text (18%) down to {@link #earthViewPercentage}</li>
     *     <li>brown earth from left to right and {@link #earthViewPercentage} to the bottom</li>
     * </ul>
     * background.
     * @param canvas drawing area
     */
    void drawBackground(Canvas canvas) {
        canvas.drawRect(0, 0, width, height, blackBackgroundPaint); //black background
        canvas.drawRect(0, hp2p(18, 0), width, hp2p(earthViewPercentage, 0), skyPaint); //blue sky
        canvas.drawRect(0, hp2p(earthViewPercentage, 0), width, height, earthPaint); //brown earth
    }

    /**
     * Draws a grey trapezoidal foot base to stow the leg. The top lies below the percentage text (18%),
     * the bottom at the {@link #LEG_MIN_LENGTH_HEIGHT_PERCENTAGE highest possible leg position}.
     *
     * @param canvas drawing area
     */
    void drawFootBase(Canvas canvas) {
        Path footBase = new Path();

        //left
        PointF A = new PointF(wp2p(0, 5), hp2p(18, 5)),
                B = new PointF(wp2p(25, 0), A.y),
                C = new PointF(wp2p(15, 0), hp2p(27, 0)),
                D = new PointF(A.x, C.y);
        footBase.moveTo(A.x, A.y);
        footBase.lineTo(B.x, B.y);
        footBase.lineTo(C.x, C.y);
        footBase.lineTo(D.x, D.y);
        footBase.lineTo(A.x, A.y);
        canvas.drawPath(footBase, footBasePaint);

        //right
        A.x = wp2p(100, -5);
        B.x = wp2p(75, 0);
        C.x = wp2p(85, 0);
        D.x = A.x;
        footBase.moveTo(A.x, A.y);
        footBase.lineTo(B.x, B.y);
        footBase.lineTo(C.x, C.y);
        footBase.lineTo(D.x, D.y);
        footBase.lineTo(A.x, A.y);
        canvas.drawPath(footBase, footBasePaint);
    }

    /**
     * Draws a horizontally centered light grey leg of 20% width
     * from below the percentage text (18%) down to {@link #legViewPercentage}.
     * The foot is of trapezoidal shape, 70% width and drawn horizontally centered at the lower leg end.
     * The thick line at the bottom of the foot is color-dependent on {@link #hasGroundContact}.
     *
     * @param canvas drawing area
     */
    void drawLeg(Canvas canvas){

        //leg
        canvas.drawRect(wp2p(40, 0), hp2p(18, 6), wp2p(60, 0), hp2p(legViewPercentage, 0), legPaint);

        //draw the foot
        Path foot = new Path();
        PointF A = new PointF(wp2p(25, -1.5f), hp2p(legViewPercentage, -25)),
                B = new PointF(wp2p(75, 1f), A.y),
                C = new PointF(wp2p(85, 1f), hp2p(legViewPercentage, 1.0f)),
                D = new PointF(wp2p(15, -1.5f), C.y);
        foot.moveTo(A.x, A.y);
        foot.lineTo(B.x, B.y);
        foot.lineTo(C.x, C.y);
        foot.lineTo(D.x, D.y);
        foot.lineTo(A.x, A.y);
        canvas.drawPath(foot, footPaint);

        //foot line
        float footLineMargin = 17;
        canvas.drawLine(wp2p(0, footLineMargin), hp2p(legViewPercentage, 0),
                wp2p(100, -footLineMargin), hp2p(legViewPercentage, 0),
                footPaint);
    }

    /**
     * Draws the {@link #legHeightPercentage leg height percentage} text horizontally centered,
     * a frame with  {@link #setLegState(AthlasLeg.LegState) leg state dependent} coloring,
     * purple view borders and the maximum leg length line at {@link #LEG_MAX_LENGTH_HEIGHT_PERCENTAGE}.
     * @param canvas drawing area
     */
    void drawFrame(Canvas canvas){
        //Leg height percentage text
        canvas.drawText(String.valueOf((int) legHeightPercentage) + "%", wp2p(50, 0), hp2p(10, 0) - (textPaint.descent() + textPaint.ascent())/2, textPaint);

        //frame rectangle
        canvas.drawRect(wp2p(0, 5), hp2p(18, 3), wp2p(100, -5), hp2p(100, -5), legStatePaint); //leg state dependent colored frame
        canvas.drawRect(0, 0, width, height, purpleLinePaint); //View borders rectangle

        //max leg travel line
        float horizonLineMargin = 6;
        canvas.drawLine(wp2p(0, horizonLineMargin), hp2p(LEG_MAX_LENGTH_HEIGHT_PERCENTAGE, 0), wp2p(100, -horizonLineMargin),
                hp2p(LEG_MAX_LENGTH_HEIGHT_PERCENTAGE, 0),
                maxLegLengthPaint);
    }

    /**
     * Calculates, if foot has ground contact. A suitable sensor reading might be used for
     * ground contact detection, if available.
     *
     * @param legViewPercentage Leg height in view height percentage
     * @param earthViewPercentage Earth height in view height percentage
     * @return true, if foot as ground contact <br>
     *     false, otherwise
     * @see #setFootGroundContact(boolean)
     * @see #setLegHeightPercentage(float)
     * @see #setEarthHeightPercentage(float)
     */
    private boolean hasGroundContact(float legViewPercentage, float earthViewPercentage){
        return (legViewPercentage > earthViewPercentage);
    }

    /**
     * Sets the leg height as of leg height range percentage. The given value is converted to
     * view height percentage and stored in {@link #legViewPercentage}. The view is redrawn and
     * {@link #hasGroundContact(float, float) ground contact} is checked.
     * @param legHeightPercentage Leg height range percentage to be displayed, 0: leg up, 100: leg extended.
     */
    public void setLegHeightPercentage(float legHeightPercentage){

        this.legHeightPercentage = Math.round(legHeightPercentage);
        float newLegViewPercentage = LEG_MIN_LENGTH_HEIGHT_PERCENTAGE + 0.01f*legHeightPercentage*(LEG_MAX_LENGTH_HEIGHT_PERCENTAGE - LEG_MIN_LENGTH_HEIGHT_PERCENTAGE);

        setFootGroundContact(hasGroundContact(newLegViewPercentage, earthViewPercentage));

        if(hasGroundContact) this.legViewPercentage = earthViewPercentage;
        else if(newLegViewPercentage >= LEG_MIN_LENGTH_HEIGHT_PERCENTAGE) this.legViewPercentage = newLegViewPercentage;
        else this.legViewPercentage = LEG_MIN_LENGTH_HEIGHT_PERCENTAGE;

        invalidate();
    }

    /**
     * Sets the earth height as of earth height range percentage. The given value is converted to
     * view height percentage and stored in {@link #earthViewPercentage}. The view is redrawn and
     * {@link #hasGroundContact(float, float) gorund contact} is checked.
     * @param earthHeightPercentage Earth height range percentage to be displayed <br>
     *                              0: ground out of sensor range, <br>
     *                              100: ground at max. leg height, <br>
     *                              >100: ground inside leg height range (ground contact possible)
     */
    public void setEarthHeightPercentage(float earthHeightPercentage){

        this.earthHeightPercentage = Math.round(earthHeightPercentage);
        float newEarthViewPercentage = LEG_MAX_LENGTH_HEIGHT_PERCENTAGE + 0.01f*(100.0f - earthHeightPercentage)*(EARTH_LOWER_BOUND_PERCENTAGE - LEG_MAX_LENGTH_HEIGHT_PERCENTAGE);

        setFootGroundContact(hasGroundContact(legViewPercentage, newEarthViewPercentage));

        if(hasGroundContact) this.earthViewPercentage = legViewPercentage;
        else this.earthViewPercentage = newEarthViewPercentage;

        invalidate();
    }

    /**
     * Sets {@link #hasGroundContact} and the color of {@code footPaint}. The view is redrawn.
     * May be used in connection with a a ground contact sensor. <br>
     * Green: Foot has ground contact <br>
     * Grey: Foot does not have ground contact
     *
     * @param isTouching true: foot has ground contact, false: otherwise
     */
    @SuppressWarnings("deprecation")
    public void setFootGroundContact(boolean isTouching){
        hasGroundContact = isTouching;

        if(hasGroundContact) footPaint.setColor(res.getColor(R.color.colorGreen));
        else footPaint.setColor(res.getColor(R.color.colorSwing));

        invalidate();
    }

    /**
     * Adjusts the color of the {@code legStatePaint}. The view is redrawn. <br>
     * <ul>
     *     <li>{@link AthlasLeg.LegState#Error Error}: Red</li>
     *     <li>{@link AthlasLeg.LegState#Locked Locked}: Green</li>
     *     <li>{@link AthlasLeg.LegState#Moving Moving}: Orange</li>
     * </ul>
     *
     * @param legState The leg state to be displayed
     */
    @SuppressWarnings("deprecation")
    public void setLegState(AthlasLeg.LegState legState){
        this.legState = legState;

        switch (legState){
            case Error:
                legStatePaint.setColor(res.getColor(R.color.colorRed));
                break;
            case Locked:
                legStatePaint.setColor(res.getColor(R.color.colorGreen));
                break;
            case Moving:
                legStatePaint.setColor(res.getColor(R.color.colorWarning));
                break;
        }

        invalidate();
    }

    /**
     * Updates {@code width} and {@code height}.
     */
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        width = w;
        height = h;

        displayMetrics = res.getDisplayMetrics();
    }

    /**
     * Converts from view width percentage to pixels for intuitive drawing. Optionally,
     * an offset independent from view size can be included.
     *
     * @param widthPercentage View width percentage, 0: left border, 100: right border
     * @param offsetDpi Offset in screen dpi
     * @return View width coordinate in pixels
     */
    float wp2p(float widthPercentage, float offsetDpi){
        float offPx;
        if (offsetDpi != 0 ) offPx = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, offsetDpi, displayMetrics);
        else offPx = 0.0f;
        return ((widthPercentage/100)*width + offPx);
    }

    /**
     * Converts from view height percentage to pixels for intuitive drawing. Optionally,
     * an offset independent from view size can be included.
     *
     * @param heightPercentage View height percentage, 0: top border, 100: bottom border
     * @param offsetDpi Offset in screen dpi
     * @return View height coordinate in pixels
     */
    float hp2p(float heightPercentage, float offsetDpi){
        float offPx;
        if (offsetDpi != 0 ) offPx = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, offsetDpi, displayMetrics);
        else offPx = 0.0f;
        return ((heightPercentage/100)*height + offPx);
    }
}
