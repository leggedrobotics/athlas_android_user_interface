/******************************************************************************
 * Copyright (c) 2016 All Rights Reserved, http://www.rsl.ethz.ch             *
 * ETH Zurich                                                                 *
 * Robotic Systems Lab                                                        *
 * Leonhardstrasse 21, LEE J201                                               *
 * 8092 Zurich                                                                *
 * Switzerland                                                                *
 * ---                                                                        *
 * Contributors:                                                              *
 * - Samuel Bachmann (samuel.bachmann@gmail.com)                              *
 * -                                                                          *
 ******************************************************************************/

package com.ATHLAS.operator_terminal.ros.nodes;

import com.ATHLAS.operator_terminal.tools.SnackbarMessage;
import com.ATHLAS.operator_terminal.listener.NodeFeedbackListener;

import org.ros.namespace.GraphName;
import org.ros.node.ConnectedNode;
import org.ros.node.Node;
import org.ros.node.NodeMain;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * ROS node template, used for the {@link OperatorTerminalNode}.
 */
public class CustomNode implements NodeMain {

  /***************************************************************************/
  /** Constants                                                             **/
  /***************************************************************************/

  private final String TAG = "CustomNode";

  /***************************************************************************/
  /** Variables                                                             **/
  /***************************************************************************/

  protected ConnectedNode nodeMain = null;
  protected String nodeName_ = "";

  protected NodeFeedbackListener nodeFeedbackListener_ = null;

  private AtomicBoolean isServiceCallFinished_ = new AtomicBoolean(false);
  final private Object lockServiceCall_ = new Object();

  /***************************************************************************/
  /** Constructor                                                           **/
  /***************************************************************************/

  public CustomNode(String nodeName) {
    nodeName_ = nodeName;
  }

  /***************************************************************************/
  /** Node Name                                                             **/
  /***************************************************************************/

  @Override
  public GraphName getDefaultNodeName() {
    return GraphName.of(nodeName_);
  }

  /***************************************************************************/
  /** Start Node                                                            **/
  /***************************************************************************/

  @Override
  public void onStart(ConnectedNode connectedNode) {
    nodeMain = connectedNode;
  }

  /***************************************************************************/
  /** Accessors                                                             **/
  /***************************************************************************/

  public void setNodeFeedbackListener(NodeFeedbackListener listener) {
    nodeFeedbackListener_ = listener;
  }

  /***************************************************************************/
  /** Methods                                                               **/
  /***************************************************************************/

  protected void sendNodeFeedback(SnackbarMessage snackbarMessage) {
    if (nodeFeedbackListener_ != null) {
      nodeFeedbackListener_.onSnackbar(snackbarMessage);
    }
  }

  protected void sendNodeFeedback(String message) {
    sendNodeFeedback(new SnackbarMessage(message));
  }

  protected void sendNodeFeedback(String message, int duration) {
    sendNodeFeedback(new SnackbarMessage(message, duration));
  }

  /***************************************************************************/
  /** Shutdown Handling                                                     **/
  /***************************************************************************/

  @Override
  public void onShutdown(Node node) {
  }

  @Override
  public void onShutdownComplete(Node node) {
  }

  /***************************************************************************/
  /** Error Handling                                                        **/
  /***************************************************************************/

  @Override
  public void onError(Node node, Throwable throwable) {
  }
}
