package com.ATHLAS.operator_terminal.ui;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ATHLAS.operator_terminal.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Fragment controlling four {@link LegStatusView} in front of a helicopter shape. Also takes care
 * of leg and earth height limits.
 *
 * @author Flurin Schwerzmann
 */

public class LegDisplayFragment extends Fragment {

    View rootView;

    /**
     * Stores the four leg displays in clockwise order beginning with the front left position. <br>
     *
     * <b>Attention:</b> Position 0 is empty (null)! Only use positions 1 to 4.
     */
    private List<LegStatusView> legViews; //starting clockwise from front left

    MainActivity mainActivity;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        rootView = inflater.inflate(R.layout.fragment_leg_display, container, false);
        mainActivity = (MainActivity) getActivity();

        setUpLegStatusDisplay();

        return rootView;
    }

    /**
     * Initialize {@link #legViews} and load leg/earth height limits from {@link SharedPreferences}.
     * Error messages are shown on the {@link TextStatusFragment text status fragment}, if loading
     * limits fails.
     */
    private void setUpLegStatusDisplay() {
        legViews = new ArrayList<>();
        for(int i = 1; i <= 5; i++) legViews.add(null);
        legViews.set(1, (LegStatusView) rootView.findViewById(R.id.leg_status_display_1));
        legViews.set(2, (LegStatusView) rootView.findViewById(R.id.leg_status_display_2));
        legViews.set(3, (LegStatusView) rootView.findViewById(R.id.leg_status_display_3));
        legViews.set(4, (LegStatusView) rootView.findViewById(R.id.leg_status_display_4));
    }

    /**
     * Update the brake state visualization of the {@link #legViews}.
     *
     * @param legs Leg data to be displayed.
     * @see LegStatusView#setLegState(AthlasLeg.LegState)
     */
    public void showBrakeStates(ArrayList<AthlasLeg> legs){
        AthlasLeg.LegState legState = AthlasLeg.LegState.Error;
        AthlasLeg leg;

        for(int legNr = 1; legNr <= 4; legNr++){
            leg = legs.get(legNr);
            switch(leg.brakeState){
                case Close:
                    legState = AthlasLeg.LegState.Locked;
                    break;
                case Open:
                    legState = AthlasLeg.LegState.Moving;
                    break;
            }
            legViews.get(legNr).setLegState(legState);
        }
    }

    /**
     * Update the leg height visualization of the {@link #legViews}.
     * The {@link AthlasLeg#legHeight raw leg height} is converted to percentage beforehand.
     *
     * @param legs Leg data to be displayed.
     * @see LegStatusView#setLegHeightPercentage(float)
     * @see #getLegHeightPercentage(float)
     */
    public void showLegsHeight(ArrayList<AthlasLeg> legs){
        float legHeightPercentage;
        AthlasLeg leg;

        for(int legNr = 1; legNr <= 4; legNr++){
            leg = legs.get(legNr);
            legHeightPercentage = getLegHeightPercentage(leg.legHeight);
            legViews.get(legNr).setLegHeightPercentage(legHeightPercentage);
        }
    }

    /**
     * Update the earth height visualization of the {@link #legViews}.
     * The {@link AthlasLeg#earthHeight raw earth height} is converted to percentage beforehand.
     *
     * @param legs Leg data to be displayed.
     * @see LegStatusView#setEarthHeightPercentage(float)
     * @see #getEarthHeightPercentage(float)
     */
    public void showEarthHeight(ArrayList<AthlasLeg> legs){
        float earthHeightPercentage;
        AthlasLeg leg;

        for(int legNr = 1; legNr <= 4; legNr++){
            leg = legs.get(legNr);
            earthHeightPercentage = getEarthHeightPercentage(leg.earthHeight);
            legViews.get(legNr).setEarthHeightPercentage(earthHeightPercentage);
        }
    }

    /**
     * Update the leg force visualization of the {@link #legViews}, currently simply a
     * status/error messages on the {@link TextStatusFragment}.
     *
     * @param legs Leg data to be displayed.
     * @see AthlasLeg#legForceState
     */
    public void showLegsForce(ArrayList<AthlasLeg> legs){
        AthlasLeg leg;

        for(int legNr = 1; legNr <= 4; legNr++){
            leg = legs.get(legNr);
            switch(leg.legForceState){
                case Normal:
                    break;
                case Warning:
                    mainActivity.textStatusFragment.pushStatus("LEG " + leg.legNr + " LD WARN");
                    break;
                case Overload:
                    mainActivity.textStatusFragment.pushError("LEG " + leg.legNr + " OVLD");
                    break;
            }
        }
    }

    /**
     * Convert raw {@link AthlasLeg#legHeight leg height} to a percentage value based on
     * {@code legLowerLimitF} and {@code legUpperLimitF}.
     *
     * @param legHeight The raw leg height to be converted
     * @return Leg height range percentage
     * @see #setUpLegStatusDisplay()
     */
    private float getLegHeightPercentage(float legHeight){
        return 100.0f*(legHeight - mainActivity.legUpperLimitF)/
                      (mainActivity.legLowerLimitF - mainActivity.legUpperLimitF);
    }

    /**
     * Convert raw {@link AthlasLeg#earthHeight earth height} to a percentage value based on
     * {@code earthUpperLimitF} and {@code earthLowerLimitF}.
     *
     * @param earthHeight The raw earth height to be converted
     * @return Earth height range percentage
     * @see #setUpLegStatusDisplay()
     */
    private float getEarthHeightPercentage(float earthHeight){
        return 100.0f*(mainActivity.earthLowerLimitF - earthHeight)/
                      (mainActivity.earthLowerLimitF - mainActivity.earthUpperLimitF);
    }


}
