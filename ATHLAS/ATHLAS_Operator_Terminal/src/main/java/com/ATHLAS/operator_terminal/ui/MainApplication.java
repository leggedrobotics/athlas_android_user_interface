package com.ATHLAS.operator_terminal.ui;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.Menu;

import com.ATHLAS.operator_terminal.R;

import org.ros.node.NodeConfiguration;
import org.ros.node.NodeMainExecutor;

/**
 * Stores global variables.
 *
 * @author Samuel Bachmann
 */
public class MainApplication extends Application {

  private NodeMainExecutor nodeMainExecutor = null;

  private NodeConfiguration nodeConfiguration = null;

  private boolean isConnected = false;

  private String rosMasterUri = "";

  private Menu toolbarMenu = null;

  @Override
  public void onCreate() {
    super.onCreate();
    // handle first app start after installation
    SharedPreferences settings =
        getSharedPreferences("UserDetails", Context.MODE_PRIVATE);
    if (settings.getBoolean("first_app_start", true)) {
      // setup default preferences
      SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
      sharedPreferences.edit().putString(this.getString(R.string.KEY_ROS_MASTER_IP),
          this.getString(R.string.default_ros_master_ip)).apply();
      sharedPreferences.edit().putString(this.getString(R.string.KEY_ROS_MASTER_PORT),
          this.getString(R.string.default_ros_master_port)).apply();
      sharedPreferences.edit().putString(getString(R.string.KEY_LEG_LIMITS_UPPER), getString(R.string.preferences_leg_limits_default_upper))
                              .putString(getString(R.string.KEY_LEG_LIMITS_LOWER), getString(R.string.preferences_leg_limits_default_lower))
                              .putString(getString(R.string.KEY_EARTH_LIMITS_LOWER), getString(R.string.preferences_earth_limits_default_lower))
                              .putString(getString(R.string.KEY_EARTH_LIMITS_UPPER), getString(R.string.preferences_earth_limits_default_upper))
                              .putString(getString(R.string.KEY_FORCE_LIMITS_LOWER), getString(R.string.preferences_force_limits_default_lower))
                              .putString(getString(R.string.KEY_FORCE_LIMITS_WARNING), getString(R.string.preferences_force_limits_default_warning))
                              .putString(getString(R.string.KEY_FORCE_LIMITS_UPPER), getString(R.string.preferences_force_limits_default_upper))
                              .putString(getString(R.string.KEY_BAT_MIN), getString(R.string.preferences_battery_default_min))
                              .putString(getString(R.string.KEY_BAT_MAX), getString(R.string.preferences_battery_default_max))
                        .apply();

      // first app start done
      settings.edit().putBoolean("first_app_start", false).apply();
    }
  }

  public NodeMainExecutor getNodeMainExecutor() {
    return nodeMainExecutor;
  }

  public void setNodeMainExecutor(NodeMainExecutor nodeMainExecutor) {
    this.nodeMainExecutor = nodeMainExecutor;
  }

  public NodeConfiguration getNodeConfiguration() {
    return nodeConfiguration;
  }

  public void setNodeConfiguration(NodeConfiguration nodeConfiguration) {
    this.nodeConfiguration = nodeConfiguration;
  }

  public boolean isConnected() {
    return isConnected;
  }

  public void setConnected(boolean isConnected) {
    this.isConnected = isConnected;
  }

  public void setRosMasterUri(String rosMasterUri) {
    this.rosMasterUri = rosMasterUri;
  }
}
