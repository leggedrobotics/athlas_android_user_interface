package com.ATHLAS.operator_terminal.ui;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;
import android.util.Log;

import com.ATHLAS.operator_terminal.R;

public class NestedPreferenceFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

  public static final int NESTED_SCREEN_1_KEY = 1;
  public static final int NESTED_SCREEN_2_KEY = 2;

  private static final String TAG_KEY = "NESTED_KEY";
  private static final String TAG = "nestedFragment";


  public static NestedPreferenceFragment newInstance(int key) {
    NestedPreferenceFragment fragment = new NestedPreferenceFragment();

    Bundle args = new Bundle();
    args.putInt(TAG_KEY, key);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    Log.d("nestedFragment", "onCreate");
    super.onCreate(savedInstanceState);
    checkPreferenceResource();
  }

  @Override
  public void onAttach(Activity activity) {
    super.onAttach(activity);
    Log.d("nestedFragment", "onAttach");
  }

  @Override
  public void onResume() {
    super.onResume();
    getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
  }

  @Override
  public void onPause() {
    super.onPause();
    getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
  }

  @Override
  public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
    boolean value = sharedPreferences.getBoolean(key, false);
    Log.d("nested", "key: " + key + " = " + value);

  }

  private void checkPreferenceResource() {
    int key = getArguments().getInt(TAG_KEY);
    // Load the preferences from an XML resource
    Log.d("nested", "checkPreferenceResource() key: " + key);

    switch (key) {
      case NESTED_SCREEN_1_KEY:
        addPreferencesFromResource(R.xml.preferences_competition_selection);

        PreferenceScreen screen = this.getPreferenceScreen();

        CheckBoxPreference checkBoxPref = new CheckBoxPreference(screen.getContext());
        checkBoxPref.setKey("key");
        checkBoxPref.setTitle("title");
        screen.addPreference(checkBoxPref);

        break;
      case NESTED_SCREEN_2_KEY:
        break;
      default:
        break;
    }
  }
}