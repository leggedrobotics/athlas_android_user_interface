package com.ATHLAS.operator_terminal.ui;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ATHLAS.operator_terminal.R;


/**
 * Holds the {@link BatteryProgressView} via the XML-file {@code fragment_battery}.
 * Gets the battery percentage with {@link #updateBatteryVoltage(float)}  and displays it as
 * {@code String}.
 *
 * @author Luca Vandeventer
 * @author Flurin Schwerzmann
 */

public class BatteryFragment extends Fragment {

    View rootView;
    private TextView batteryPercentageTV;
    private BatteryProgressView batteryLevelView;

    MainActivity mainActivity;

    int batteryPercentage;
    float batMaxVolt, batMinVolt;

    /**
     * Layout inflation, {@code View} variable initialization and voltage limit initialization.
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        rootView = inflater.inflate(R.layout.fragment_battery, container, false);
        mainActivity = (MainActivity) getActivity();

        batteryPercentageTV = (TextView) rootView.findViewById(R.id.Battery_Percentage);
        batteryLevelView = (BatteryProgressView) rootView.findViewById(R.id.battery_level_view);

        loadBatteryVoltageLimits();

        updateBatteryVoltage(4.0f);

        return rootView;
    }

    /**
     * Sets the battery percentage for the {@link #batteryLevelView} and the {@code String} for the
     * {@link #batteryPercentageTV}. <br>
     * This is done with:
     * <ul>
     *     <li>{@link #voltageToPercentage(float)} to convert from raw voltage to percentage (using limits)</li>
     *     <li>{@link BatteryProgressView#setBatteryLevel(int)} to set percentage</li>
     *     <li>{@link TextView#setText(CharSequence)} to display the percentage as String</li>
     * </ul>
     *
     * @param batteryVoltage absolute battery voltage
     */
    public void updateBatteryVoltage(float batteryVoltage){
        batteryPercentage = Math.round(voltageToPercentage(batteryVoltage));

        final String percentageString = batteryPercentage + "%";
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                batteryLevelView.setBatteryLevel(batteryPercentage);
                batteryPercentageTV.setText(percentageString);
            }
        });
    }

    /**
     * Converts from raw battery voltage to a percentage value.
     *
     * @param batteryVoltage The battery voltage sensor reading to be converted.
     * @return Battery capacity percentage (as defined by limits).
     */
    private float voltageToPercentage(float batteryVoltage){
        return 100.0f*(batMinVolt - batteryVoltage)/(batMinVolt - batMaxVolt);
    }

    /**
     * Get battery discharge limits from settings. An error "BAT LIM NOT AVAIL" is shown on the
     * {@link TextStatusFragment}, if limits could not be parsed.
     */
    private void loadBatteryVoltageLimits(){
        Resources res = getResources();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        try {
            batMaxVolt = Float.parseFloat(sharedPreferences.getString(res.getString(R.string.KEY_BAT_MAX), "4.2"));
            batMinVolt = Float.parseFloat(sharedPreferences.getString(res.getString(R.string.KEY_BAT_MIN), "3.27"));

        } catch (NumberFormatException e){
            mainActivity.textStatusFragment.pushError("BAT LIM NOT AVAIL");
        }
    }

}
