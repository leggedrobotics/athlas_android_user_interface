package com.ATHLAS.operator_terminal.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.SoundEffectConstants;
import android.view.View;

import com.ATHLAS.operator_terminal.R;
import com.ATHLAS.operator_terminal.ros.nodes.OperatorTerminalNode;

/**
 * Creates a movable {@link ControlLeverView#lever} and six different buttons
 * ({@link ControlLeverView#up}, {@link ControlLeverView#park},
 * {@link ControlLeverView#high}, {@link ControlLeverView#auto},
 * {@link ControlLeverView#low}, {@link ControlLeverView#emer})
 * on which the lever can be dragged onto, along a given path. If a button is selected
 * {@link OperatorTerminalNode#operateLeg} is called,
 * which then sets the legs accordingly.
 *
 * @author Luca Vandeventer
 */
public class ControlLeverView extends View {
    /**
     * Makes sure sound is only played once
     */
    boolean soundNotPlayed = false;

    /**
     * Creates Rectangle which can be dragged onto a button along a given path
     */
    private RectF lever;

    /**
     * Creates all Buttons
     */
    private RectF up,
                park,
                high,
                auto,
                low,
                emer;

    int width, height = 0;

    /**
     * Graphical positions, which are used to place the lever, the path on which the
     * lever moves, the path conditions and the buttons.
     */
    float leverWidth,
            leverHeight,
            halfLeverWidth,
            halfLeverHeight,
            spaceBtwButtons,
            halfSpaceBtwButtons,
            vertL,
            vertR,
            endRect,
            upTop,
            upBottom,
            parkTop,
            parkBottom,
            highTop,
            highBottom,
            autoTop,
            autoBottom,
            lowTop,
            lowBottom,
            emerTop,
            emerBottom,
            startHorizontal,
            endHorizontal,
            endMin,
            emer10,
            margin,
            vertPathL,
            endButtons,
            radialOffsetW,
            radialOffsetH,
            centerText,
            width10,
            height1,
            height20,
            textSize;

    private Point leverPoint;

    private Paint leverPaint,
            pathCondPaint,
            greyLine,
            btnPaint,
            btnNamePaint,
            btnClickedPaint,
            btnClickedTextPaint;

    DisplayMetrics displayMetrics;

    /**
     * Labels to check which button is active <br> <br>
     * <b>{@code Moving:}</b> no button is active, lever is being moved <br>
     * <b>{@code Up:}</b> lever is set on {@code Up}, button {@code Up} is active <br>
     * <b>{@code Park:}</b> lever is set on {@code Park}, button {@code Park} is active <br>
     * <b>{@code High:}</b> lever is set on {@code High}, button {@code High} is active <br>
     * <b>{@code Auto:}</b> lever is set on {@code Auto}, button {@code Auto} is active <br>
     * <b>{@code Low:}</b> lever is set on {@code Low}, button {@code Low} is active <br>
     * <b>{@code Emer:}</b> lever is set on {@code Emer}, button {@code Emer} is active <br>
     */
    private enum LegButton {Moving, Up, Park, High, Auto, Low, Emer}
    private LegButton currentLeverPosition = LegButton.Moving;

    MainActivity mainActivity;
    OperatorTerminalNode operatorTerminalNode;

    private static final String TAG = "ControlSurfaceView";

    public ControlLeverView(Context context, AttributeSet attrs) {
        super(context, attrs);

        initPaint();
        setFocusable(true);

        mainActivity = (MainActivity) context;
        operatorTerminalNode = mainActivity.operatorTerminalNode;
    }

    /**
     * Initializes all {@code Paint} objects.
     */
    @SuppressWarnings("deprecation")
    private void initPaint(){
        leverPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        leverPaint.setStyle(Paint.Style.FILL);
        leverPaint.setColor(Color.rgb(207,72,37)); //Athlas Red

        pathCondPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        pathCondPaint.setColor(Color.YELLOW);

        greyLine = new Paint(Paint.ANTI_ALIAS_FLAG);
        greyLine.setColor(Color.GRAY);

        btnPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        btnPaint.setColor(getResources().getColor(R.color.btn_grey));

        btnNamePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        btnNamePaint.setColor(Color.WHITE);
        btnNamePaint.setTextAlign(Paint.Align.CENTER);
        textSize = getResources().getDimension(R.dimen.lsd_text_size);
        btnNamePaint.setTextSize(textSize);

        btnClickedPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        btnClickedPaint.setStrokeWidth(12);
        btnClickedPaint.setStyle(Paint.Style.STROKE);
        btnClickedPaint.setColor(Color.WHITE);
        btnClickedPaint.setShadowLayer(50, 0, 0, Color.WHITE);

        btnClickedTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        btnClickedTextPaint.setColor(Color.WHITE);
        btnClickedTextPaint.setTextAlign(Paint.Align.CENTER);
        btnClickedTextPaint.setTextSize(textSize);
        btnClickedTextPaint.setTypeface(Typeface.DEFAULT_BOLD);
    }

    /**
     * Initializes all variables
     */
    private void initVars(){
//      Lever variables
        float leverSize = 7.333f;
        leverWidth = wp2p(leverSize,0);
        leverHeight = hp2p(leverSize,0);
        halfLeverWidth = leverWidth/2;
        halfLeverHeight = leverHeight/2;

//      Graphical variables
        width10 = wp2p(10,0);
        height1 = hp2p(1,0);
        height20 = hp2p(20,0);

//        PathCondition variables for y coord
        float CalcSpaceBtwButtons = (100 - 6*leverSize)/7.0f;
        spaceBtwButtons = hp2p(CalcSpaceBtwButtons,0);
        halfSpaceBtwButtons = spaceBtwButtons/2;

        upTop = spaceBtwButtons;
        upBottom = upTop + leverHeight;

        parkTop = upBottom + spaceBtwButtons;
        parkBottom = parkTop + leverHeight;

        highTop = parkBottom + spaceBtwButtons;
        highBottom = highTop + leverHeight;

        autoTop = highBottom + spaceBtwButtons;
        autoBottom = autoTop + leverHeight;

        lowTop = autoBottom + spaceBtwButtons;
        lowBottom = lowTop + leverHeight;

        emerTop = lowBottom + spaceBtwButtons;
        emerBottom = emerTop + leverHeight;

//      PathCondition variables for x coord
        vertL = wp2p(19,0);
        vertR = vertL + leverWidth;

        endRect = wp2p(50,0);

        startHorizontal = vertL + halfLeverWidth;
        endHorizontal = endRect - halfLeverWidth;
        endMin = endRect - leverWidth;

        emer10 = endRect + width10;

//      RoundRect radii
        radialOffsetW = wp2p(0,5);
        radialOffsetH = hp2p(0,5);

//      Path variables
        margin = wp2p(3,0);
        vertPathL = vertL + margin;

//      Buttons
        endButtons = wp2p(81,0);
        centerText = (endRect + endButtons)/2;

//      Generates point around which the lever is centered.
//      leverPoint is updated in the onTouchEvent method
        leverPoint = new Point();
    }

    /**
     * Initializes the {@link ControlLeverView#lever}, all buttons ({@link ControlLeverView#up},
     * {@link ControlLeverView#park}, {@link ControlLeverView#high},
     * {@link ControlLeverView#auto}, {@link ControlLeverView#low},
     * {@link ControlLeverView#emer}), and sets the lever on {@link ControlLeverView#high} and
     * the button on {@link ControlLeverView.LegButton High}.
     */
    void initRects(){
//        RectF(left, top, right, bottom)
        lever = new RectF(endRect - leverWidth, highTop - height1, endRect, highBottom + height1);
        currentLeverPosition = LegButton.High;
        up = new RectF(endRect, upTop - height1, endButtons, upBottom  + height1);
        park = new RectF(endRect, parkTop - height1, endButtons, parkBottom  + height1);
        high = new RectF(endRect, highTop - height1, endButtons, highBottom + height1);
        auto = new RectF(endRect, autoTop - height1, endButtons, autoBottom + height1);
        low = new RectF(endRect, lowTop - height1, endButtons, lowBottom + height1);
        emer = new RectF(endRect + width10, emerTop - height1, endButtons + width10, emerBottom + height1);
    }

    /**
     * Updates {@code width} and {@code height} and calls {@link ControlLeverView#initVars()} and
     * {@link ControlLeverView#initRects()}
     *
     * @see LegStatusView#onSizeChanged
     */
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        width = w;
        height = h;

        displayMetrics = getResources().getDisplayMetrics();
        initVars();
        initRects();
    }

    /**
     * Draws background color, the path condition (if needed) using
     * {@link ControlLeverView#drawPathCond(Canvas)}, the path for the lever with
     * {@link ControlLeverView#drawPath(Canvas)}, the buttons using
     * {@link ControlLeverView#drawButtons(Canvas)} and the {@link ControlLeverView#lever}.
     *
     * @param canvas drawing area
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawColor(Color.BLACK);
//        drawPathCond(canvas); //activate for visible path conditions!
        drawPath(canvas);
        drawButtons(canvas);
        canvas.drawRoundRect(lever, radialOffsetW, radialOffsetH, leverPaint);

    }

    /**
     * Draws buttons as {@code RoundRects} and if a button is active (checked in
     * {@link ControlLeverView#update(Point)}), it draws a white rectangle around it and sets
     * the Text to bold.
     * Gets called in {@link ControlLeverView#onDraw(Canvas)}.
     *
     * @param canvas drawing area
     */
    void drawButtons(Canvas canvas){
//      drawRoundRect(left, top, right, bottom, paint, float rx, float ry)
        canvas.drawRoundRect(up, radialOffsetW, radialOffsetH, btnPaint);
        canvas.drawText("UP", centerText, upBottom - height1, btnNamePaint);
        canvas.drawRoundRect(park, radialOffsetW, radialOffsetH, btnPaint);
        canvas.drawText("PARK", centerText, parkBottom - height1, btnNamePaint);
        canvas.drawRoundRect(high, radialOffsetW, radialOffsetH, btnPaint);
        canvas.drawText("HIGH", centerText, highBottom - height1, btnNamePaint);
        canvas.drawRoundRect(auto, radialOffsetW, radialOffsetH, btnPaint);
        canvas.drawText("AUTO", centerText, autoBottom - height1, btnNamePaint);
        canvas.drawRoundRect(low, radialOffsetW, radialOffsetH,btnPaint);
        canvas.drawText("LOW", centerText, lowBottom - height1, btnNamePaint);
        canvas.drawRoundRect(emer, radialOffsetW, radialOffsetH, btnPaint);
        canvas.drawText("EMER", centerText + width10, emerBottom - height1, btnNamePaint);

        switch (currentLeverPosition){
            case Moving:
                //do not draw anything special
                break;
            case Up:
                canvas.drawRoundRect(up,radialOffsetW,radialOffsetH,btnClickedPaint);
                canvas.drawText("UP", centerText, upBottom - height1, btnClickedTextPaint);
                break;
            case Park:
                canvas.drawRoundRect(park,radialOffsetW,radialOffsetH,btnClickedPaint);
                canvas.drawText("PARK", centerText, parkBottom - height1, btnClickedTextPaint);
                break;
            case High:
                canvas.drawRoundRect(high,radialOffsetW,radialOffsetH,btnClickedPaint);
                canvas.drawText("HIGH", centerText, highBottom - height1, btnClickedTextPaint);
                break;
            case Auto:
                canvas.drawRoundRect(auto,radialOffsetW,radialOffsetH,btnClickedPaint);
                canvas.drawText("AUTO", centerText, autoBottom - height1, btnClickedTextPaint);
                break;
            case Low:
                canvas.drawRoundRect(low,radialOffsetW,radialOffsetH,btnClickedPaint);
                canvas.drawText("LOW", centerText, lowBottom - height1, btnClickedTextPaint);
                break;
            case Emer:
                canvas.drawRoundRect(emer,radialOffsetW,radialOffsetH,btnClickedPaint);
                canvas.drawText("EMER", centerText + width10, emerBottom - height1, btnClickedTextPaint);
                break;
        }

    }
    /**
     * Draws the path condition in yellow paint if not commented out in the
     * {@link ControlLeverView#onDraw(Canvas)} method. The path condition shows the area the
     * {@link ControlLeverView#leverPoint} has to stay inside in order to be moved. The effective
     * conditions though are implemented in the {@link ControlLeverView#update(Point)} method and
     * should always be the same as these.
     *
     * @param canvas drawing area
     */
    void drawPathCond(Canvas canvas){
        canvas.drawRect(vertL - leverWidth, upTop + halfLeverWidth, vertR, emerBottom - halfLeverHeight, pathCondPaint); //Vertical
        canvas.drawRect(vertL, upTop - halfSpaceBtwButtons, endRect, upBottom + halfSpaceBtwButtons, pathCondPaint); //Up
        canvas.drawRect(vertL, parkTop - halfSpaceBtwButtons, endRect, parkBottom + halfSpaceBtwButtons, pathCondPaint); //Parking
        canvas.drawRect(vertL, highTop - halfSpaceBtwButtons, endRect, highBottom + halfSpaceBtwButtons, pathCondPaint); //High
        canvas.drawRect(vertL, autoTop - halfSpaceBtwButtons, endRect, autoBottom + halfSpaceBtwButtons, pathCondPaint); //Auto
        canvas.drawRect(vertL, lowTop - halfSpaceBtwButtons, endRect, lowBottom + halfSpaceBtwButtons, pathCondPaint); //Low
        canvas.drawRect(vertL, emerTop - halfSpaceBtwButtons, emer10, emerBottom + halfSpaceBtwButtons, pathCondPaint); //Emer
    }

    /**
     * Draws the visual path for the user to know along where the lever can be dragged.
     *
     * @param canvas drawing area
     */
    void drawPath(Canvas canvas){
        canvas.drawRect(vertPathL, upTop + margin, vertR - margin, emerBottom - margin, greyLine); //Vertical
        canvas.drawRect(vertPathL, upTop + margin, endRect, upBottom - margin, greyLine); //Up
        canvas.drawRect(vertPathL, parkTop + margin, endRect, parkBottom - margin, greyLine); //Parking
        canvas.drawRect(vertPathL, highTop + margin, endRect, highBottom - margin, greyLine); //High
        canvas.drawRect(vertPathL, autoTop + margin, endRect, autoBottom - margin, greyLine); //Auto
        canvas.drawRect(vertPathL, lowTop + margin, endRect, lowBottom - margin, greyLine); //Low
        canvas.drawRect(vertPathL, emerTop + margin, emer10, emerBottom - margin, greyLine); //Emer
    }

    /**
     * Sets the {@link ControlLeverView#leverPoint} to the actual finger position.
     * Then checks if {@link ControlLeverView#leverPoint} is being moved and if the
     * {@link ControlLeverView#leverPoint} is within some bounds using
     * {@link ControlLeverView#inRect(Point)} . If both conditions are fulfilled the
     * position is updated using {@link ControlLeverView#update(Point)}.
     * At the end the {@code View} is invalidated.
     *
     * @param event Motion event from user on device eg. finger movement
     * @return true
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
//        Point is set to finger location
        leverPoint.set((int)event.getX(), (int)event.getY());
        if(event.getAction() == MotionEvent.ACTION_MOVE && inRect(leverPoint))
            update(leverPoint);
        invalidate();

        return true;
    }

    /**
     * Updates the {@link ControlLeverView#lever} to be centered around the
     * {@link ControlLeverView#leverPoint}, if the conditions are fulfilled. The conditions are
     * necessary for the {@link ControlLeverView#lever} not to jump from one position to another.
     * It checks as well if the {@link ControlLeverView#lever} is dragged to any button. If it is,
     * the {@link ControlLeverView#lever} is set next to the button, the button gets activated
     * with {@link ControlLeverView.LegButton LegButton} over the method
     * {@link ControlLeverView#drawButtons(Canvas)}, a click sound is played and
     * the {@link OperatorTerminalNode#operateLeg} gets called. If it isn't, the
     * {@link ControlLeverView#lever} is set on
     * {@link ControlLeverView.LegButton LegButton.Moving} and the
     * {@link ControlLeverView#lever} is allowed to slide along certain bounds
     *
     * @param p {@link ControlLeverView#leverPoint}
     */
    public void update(Point p){
//        set(left, top, right, bottom)
//        vertical path: conditions?
        if(p.x >= vertL - leverWidth && p.x <= vertR && p.y >= upTop + halfLeverHeight && p.y <= emerBottom - halfLeverHeight) {
            lever.set(vertL, p.y - halfLeverHeight - height1, vertL + leverWidth, p.y + halfLeverHeight + height1);

        }
//        'up' horizontal path
        if(p.y >= upTop - halfSpaceBtwButtons && p.y <= upBottom + halfSpaceBtwButtons && p.x >= startHorizontal && p.x <= endButtons) {
            if (p.x > endHorizontal) {
                lever.set(endMin, upTop - height1, endRect, upBottom + height1);
                currentLeverPosition = LegButton.Up;
                if (soundNotPlayed){
                    this.playSoundEffect(SoundEffectConstants.CLICK);
                    soundNotPlayed = false;
                }

                operatorTerminalNode.operateLeg(AthlasLeg.LegCmd.Up, 0);
            }
            else {
                lever.set(p.x - halfLeverWidth, upTop - height1, p.x + halfLeverWidth, upBottom + height1);
                currentLeverPosition = LegButton.Moving;
                soundNotPlayed = true;
            }
        }
//        'parking' horizontal path
        if(p.y >= parkTop - halfSpaceBtwButtons && p.y <= parkBottom + halfSpaceBtwButtons && p.x >= startHorizontal && p.x <= endButtons) {
            if (p.x > endHorizontal) {
                lever.set(endMin, parkTop - height1, endRect, parkBottom + height1);
                currentLeverPosition = LegButton.Park;
                if (soundNotPlayed){
                    this.playSoundEffect(SoundEffectConstants.CLICK);
                    soundNotPlayed = false;
                }

                operatorTerminalNode.operateLeg(AthlasLeg.LegCmd.Parking, 0);
            }
            else {
                lever.set(p.x - halfLeverWidth, parkTop - height1, p.x + halfLeverWidth, parkBottom + height1);
                currentLeverPosition = LegButton.Moving;
                soundNotPlayed = true;
            }
        }
//        'high' horizontal path
        if(p.y >= highTop - halfSpaceBtwButtons && p.y <= highBottom + halfSpaceBtwButtons && p.x >= startHorizontal && p.x <= endButtons) {
            if (p.x > endHorizontal) {
                lever.set(endMin, highTop - height1, endRect, highBottom + height1);
                currentLeverPosition = LegButton.High;
                if (soundNotPlayed){
                    this.playSoundEffect(SoundEffectConstants.CLICK);
                    soundNotPlayed = false;
                }

                operatorTerminalNode.operateLeg(AthlasLeg.LegCmd.High, 0);
            }
            else {
                lever.set(p.x - halfLeverWidth, highTop - height1, p.x + halfLeverWidth, highBottom + height1);
                currentLeverPosition = LegButton.Moving;
                soundNotPlayed = true;
            }
        }
//        'auto' horizontal path
        if(p.y >= autoTop - halfSpaceBtwButtons && p.y <= autoBottom + halfSpaceBtwButtons && p.x >= startHorizontal && p.x <= endButtons) {
            if (p.x > endHorizontal) {
                lever.set(endMin, autoTop - height1, endRect, autoBottom + height1);
                currentLeverPosition = LegButton.Auto;
                if (soundNotPlayed){
                    this.playSoundEffect(SoundEffectConstants.CLICK);
                    soundNotPlayed = false;
                }

                operatorTerminalNode.operateLeg(AthlasLeg.LegCmd.Auto, 0);
            }
            else {
                lever.set(p.x - halfLeverWidth, autoTop - height1, p.x + halfLeverWidth, autoBottom + height1);
                currentLeverPosition = LegButton.Moving;
                soundNotPlayed = true;
            }
        }
//        'low' horizontal path
        if(p.y >= lowTop - halfSpaceBtwButtons && p.y <= lowBottom + halfSpaceBtwButtons && p.x >= startHorizontal && p.x <= endButtons) {
            if (p.x > endHorizontal) {
                lever.set(endMin, lowTop - height1, endRect, lowBottom + height1);
                currentLeverPosition = LegButton.Low;
                if (soundNotPlayed){
                    this.playSoundEffect(SoundEffectConstants.CLICK);
                    soundNotPlayed = false;
                }

                operatorTerminalNode.operateLeg(AthlasLeg.LegCmd.Low, 0);
            }
            else {
                lever.set(p.x - halfLeverWidth, lowTop - height1, p.x + halfLeverWidth, lowBottom + height1);
                currentLeverPosition = LegButton.Moving;
                soundNotPlayed = true;
            }
        }
//        'emergency' horizontal path
        if(p.y >= emerTop - halfSpaceBtwButtons && p.y <= emerBottom + halfSpaceBtwButtons && p.x >= startHorizontal && p.x <= endButtons + width10) {
            if (p.x > emer10 - halfLeverWidth) {
                lever.set(endMin + width10, emerTop - height1, emer10, emerBottom + height1);
                currentLeverPosition = LegButton.Emer;
                if (soundNotPlayed){
                    this.playSoundEffect(SoundEffectConstants.CLICK);
                    soundNotPlayed = false;
                }

                operatorTerminalNode.operateLeg(AthlasLeg.LegCmd.Emergency, 0);
            }
            else {
                lever.set(p.x - halfLeverWidth, emerTop - height1, p.x + halfLeverWidth, emerBottom + height1);
                currentLeverPosition = LegButton.Moving;
                soundNotPlayed = true;
            }
        }
    }

    /**
     * Checks if {@link ControlLeverView#leverPoint} is inside the {@code RectF} of the
     * {@link ControlLeverView#lever} plus some margin. The margin depends on where the
     * {@link ControlLeverView#lever} is (in a vertical or horizontal path). The margin
     * is set in order to allow quick {@link ControlLeverView#lever} movements, but such
     * that it can't jump between horizontal paths.
     *
     * @param point {@link ControlLeverView#leverPoint}
     * @return true, if {@link ControlLeverView#leverPoint} is inside bounds
     */
    public boolean inRect (Point point){
        RectF checkRectangle;
        if (point.x <= vertR)
            checkRectangle = new RectF(lever.left - width10, lever.top - height20, lever.right + width10, lever.bottom + height20);
        else
            checkRectangle = new RectF(lever.left - width10, lever.top - halfSpaceBtwButtons + height1, lever.right + width10, lever.bottom + halfSpaceBtwButtons - height1);

        return checkRectangle.contains(point.x, point.y);
    }

    /**
     * @see LegStatusView#wp2p
     */
    //takes a percentage of View-width and converts it to pixels
    private float wp2p(float widthPercentage, float offsetDpi){
        float offPx;
        if (offsetDpi != 0 ) offPx = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, offsetDpi, displayMetrics);
        else offPx = 0.0f;
        return ((widthPercentage/100)*width + offPx);
    }

    /**
     * @see LegStatusView#hp2p
     */
    //takes a percentage of View-height and converts it to pixels
    private float hp2p(float heightPercentage, float offsetDpi){
        float offPx;
        if (offsetDpi != 0 ) offPx = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, offsetDpi, displayMetrics);
        else offPx = 0.0f;
        return ((heightPercentage/100)*height + offPx);
    }

}
