package com.ATHLAS.operator_terminal.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.support.annotation.LayoutRes;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.ATHLAS.operator_terminal.R;

/**
 * Allows the user to set various preferences such as ROS IP and port, leg height/leg force/earth
 * and battery voltage limits.
 */
public class UserSettingActivity extends PreferenceActivity
    implements MyPreferenceFragment.Callback, SharedPreferences.OnSharedPreferenceChangeListener {

  private SharedPreferences.OnSharedPreferenceChangeListener mListener;
  private AppCompatDelegate mDelegate;

  private static final String TAG_NESTED = "TAG_NESTED";

  @Override
  protected void onCreate(Bundle savedInstanceState) {

    getDelegate().installViewFactory();
    getDelegate().onCreate(savedInstanceState);
    super.onCreate(savedInstanceState);
    setToolbar();

    if (savedInstanceState == null) {
      getFragmentManager().beginTransaction()
          .add(R.id.settings_content_frame, new MyPreferenceFragment())
          .commit();
    }
  }

  @Override
  public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
  }

  @Override
  public void onBackPressed() {
    if (getFragmentManager().getBackStackEntryCount() == 0) {
      super.onBackPressed();
    } else {
      getFragmentManager().popBackStack();
      getSupportActionBar()
          .setTitle(getApplicationContext().getString(R.string.action_settings));
    }
  }

  @Override
  public void onNestedPreferenceSelected(int key) {
    Log.d("userSettingsAct", "key: " + key);

    switch (key) {
      default:
        getSupportActionBar().setTitle("Settings");
        break;
    }
    getFragmentManager().beginTransaction()
        .replace(R.id.settings_content_frame, NestedPreferenceFragment.newInstance(key), TAG_NESTED)
        .addToBackStack(TAG_NESTED).commit();
  }

  @Override
  protected void onPostCreate(Bundle savedInstanceState) {
    super.onPostCreate(savedInstanceState);
    getDelegate().onPostCreate(savedInstanceState);
  }

  @Override
  public MenuInflater getMenuInflater() {
    return getDelegate().getMenuInflater();
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == android.R.id.home) {
      onBackPressed();
      return true;
    }

    return super.onOptionsItemSelected(item);
  }

  @Override
  public void setContentView(@LayoutRes int layoutResID) {
    getDelegate().setContentView(layoutResID);
  }

  @Override
  public void onResume() {
    super.onResume();
    getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
  }

  @Override
  protected void onPostResume() {
    super.onPostResume();
    getDelegate().onPostResume();
  }

  @Override
  public void onPause() {
    super.onPause();
    getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
  }

  @Override
  protected void onStop() {
    super.onStop();
    getDelegate().onStop();
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    getDelegate().onDestroy();
  }


  private void setToolbar() {
    setContentView(R.layout.activity_settings);
  }

  private ActionBar getSupportActionBar() {
    return getDelegate().getSupportActionBar();
  }


  private AppCompatDelegate getDelegate() {
    if (mDelegate == null) {
      mDelegate = AppCompatDelegate.create(this, null);
    }
    return mDelegate;
  }
}
