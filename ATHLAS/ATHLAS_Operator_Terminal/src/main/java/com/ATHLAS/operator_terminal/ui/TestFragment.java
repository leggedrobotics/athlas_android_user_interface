 package com.ATHLAS.operator_terminal.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.NumberPicker;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import com.ATHLAS.operator_terminal.R;
import com.ATHLAS.operator_terminal.ros.nodes.OperatorTerminalNode;

import athlas_msgs.AthlasEarthHeight;
import athlas_msgs.AthlasLegForce;
import athlas_msgs.AthlasLegHeight;
import athlas_msgs.OTBrakeStateChangeMsg;

 /**
  * Fragment providing test controls and displays:
  * <ul>
  *     <li>{@link #realModeSwitch Real mode switch}</li>
  *     <li>{@link #legNrPicker Leg number picker}</li>
  *     <li>{@link #tvLegUpperLimit Limits}</li>
  *     <li>{@link #brakeSwitch Brake test value switch}</li>
  *     <li>{@link #btnBrakeStateMsgTest Send ROS brake state change message}</li>
  *     <li>{@link #btnBrakeOperation Brake operation}</li>
  *     <li>{@link #btnLegUp Leg movement command buttons}</li>
  *     <li>{@link #legSeeker Seekers for leg height, earth height and leg force}</li>
  * </ul>
  *
  * @author Flurin Schwerzmann
 */

public class TestFragment extends Fragment {
    MainActivity mainActivity;

    public View rootView;
     /**
      * Widget for selecting a specific or all legs to be tested. Sets {@link #legTestNr}.
      */
    NumberPicker legNrPicker;

     /**
      * Shows limits or measurements.
      * <ul>
      *     <li>{@code tvLegUpperLimit}: Upper leg movement limit</li>
      *     <li>{@code tvLegLowerLimit}: Lower leg movement limit</li>
      *     <li>{@code tvLegHeight}: Current raw leg position value</li>
      *     <li>{@code tvMotorCurrent}: Current raw motor current/leg force value</li>
      * </ul>
      */
    TextView tvLegUpperLimit, tvLegLowerLimit, tvlegHeight, tvMotorCurrent;

     /**
      * Triggers leg commands.
      * <ul>
      *     <li>{@code btnLegUp}: Move leg to stowed position (cruise flight)</li>
      *     <li>{@code btnParking}: Move leg to highest position without helicopter fuselage ground contact</li>
      *     <li>{@code btnLegHigh}: Move leg to highest landable position</li>
      *     <li>{@code btnLegAuto}: Activate ATHLAS leg height control algorithm</li>
      *     <li>{@code btnLegLow}: Move leg to lowest landable position</li>
      *     <li>{@code btnLegEmergency}: Activate Emergency mode</li>
      * </ul>
      */
     Button btnLegUp,
            btnParking,
            btnLegHigh,
            btnLegAuto,
            btnLegLow,
            btnLegEmergency;

     /**
      * Tests the brake system.
      * <ul>
      *     <li>{@code btnBrakeOperation}: Test brake actuation</li>
      *     <li>{@code btnBrakeStateMsgTest}: Send a brake operation ROS message for connection and UI testing</li>
      * </ul>
      */
     Button btnBrakeOperation,
        btnBrakeStateMsgTest;

     /**
      * <ul>
      *     <li>{@code realModeSwitch}: true to send ROS messages, false for self-contained app usage.
      *     <li>{@code brakeSwitch}: true for <i>brake set</i>, false for <i>brake released</i>.
      *     </li>
      * </ul>
      */
    Switch realModeSwitch, brakeSwitch;

     /**
      * Sets {@link #legSeekerLegHeight leg height}, {@link #earthSeekerEarthHeight} or {@link #forceSeekerForceLevel}
      * for connection and UI testing. <br>
      * ROS functionality is as follows:
      * <ul>
      *     <li>{@code realMode=true}: A ROS service call is executed after seeker movement has ended.
      *                                The {@link LegDisplayFragment leg display} is updated thereby.</li>
      *     <li>{@code realMode=false}: The {@link LegDisplayFragment leg display} is directly set,
      *                                 independent from ROS.</li>
      * </ul>
      *
      * <b>Important:</b> The ATHLAS system is not operated through the seekers in any way.
      * Use {@link #btnLegUp leg buttons} for leg actuation.
      * @see #realModeSwitch
      * @see #operatorTerminalNode
      */
     SeekBar legSeeker, earthSeeker, forceSeeker;

     /**
      * Seeker position converted to raw value. Can be directly used for ROS communication.
      *
      * @see #legSeeker
      * @see #earthSeeker
      * @see #forceSeeker
      */
     private float legSeekerLegHeight, earthSeekerEarthHeight, forceSeekerForceLevel;

     /**
      * The number of the leg to be tested, 0: every leg, 1-4: single leg.
      *
      * @see #legNrPicker
      */
    int legTestNr = 0;


     /**
      * Connected ROS framework.
      */
    private OperatorTerminalNode operatorTerminalNode;
     
    private final String TAG = "OT Fragment";

     /**
      * Initialize variables and UI elements.
      * @see #initViews()
      * @see #setUpSwitches()
      * @see #setUpButtons()
      * @see #setUpLegNrPicker()
      * @see #setUpSeekers()
      */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);


        rootView = inflater.inflate(R.layout.fragment_test, container, false);
        mainActivity = (MainActivity) getActivity();

        initViews();
        setUpSwitches();
        setUpButtons();
        setUpLegNrPicker();
        setUpSeekers();

        return rootView;
    }

     /**
      * Updates the {@link #updateLegHeightTV() leg height}, {@link #updateLegLimitTv() leg limit}
      * and {@link #updateMotorCurrentTV() motor current} info text.
      */
     @Override
     public void onResume() {
         super.onResume();

         this.operatorTerminalNode = mainActivity.operatorTerminalNode;

         updateLegLimitTv();

         updateLegHeightTV();
         updateMotorCurrentTV();
     }

     /**
      * Publishes an {@code OTBrakeStateChangeMsg} with the selected {@link #legTestNr leg number}
      * and {@link #brakeSwitch brake position} for connection and UI testing if
      * {@link #realModeSwitch real mode} is activated. Status messages are shown, if real mode is
      * turned off or the publisher is not available.
      * <p>This message is otherwise generated by ROS whenever the brake is actuated.</p>
      *
      * @see OperatorTerminalNode#OTBrakeStateChangePublisher OTBrakeStateChangePublisher
      * @see #btnBrakeStateMsgTest
      */
    private void testOTBrakeStateChangeMsg() {
        if (operatorTerminalNode.isReal()) {
            if(operatorTerminalNode.OTBrakeStateChangePublisher != null) {
                OTBrakeStateChangeMsg brakeStateChangeMsg = operatorTerminalNode.messageFactory.newFromType(OTBrakeStateChangeMsg._TYPE);
                brakeStateChangeMsg.setLegNumber((short) legTestNr);
                brakeStateChangeMsg.setIsBrakeSet(!brakeSwitch.isChecked()); ///convert to Mechatronic's brake boolean (inverted to OT's)

                operatorTerminalNode.OTBrakeStateChangePublisher.publish(brakeStateChangeMsg);
            } else {
                mainActivity.textStatusFragment.pushStatus("BRK PUB NOT AVAIL");
            }
        } else{
            mainActivity.textStatusFragment.pushStatusRealMode(false);
        }
    }

     /**
      * Tests brake actuation according to {@link #brakeSwitch} position and
      *  {@link #legTestNr leg number} chosen.
      *
      * @see OperatorTerminalNode#operateBrakeTo(AthlasLeg.BrakeState, int)
      */
     private void testBrakeOperation(){
         AthlasLeg.BrakeState brakeCmd;
         if(brakeSwitch.isChecked()) brakeCmd = AthlasLeg.BrakeState.Close;
         else brakeCmd = AthlasLeg.BrakeState.Open;

         operatorTerminalNode.operateBrakeTo(brakeCmd, legTestNr);
     }

     /**
      * Publishes an {@code AthlasLegHeight} message with the selected {@link #legTestNr leg number}
      * and {@link #legSeekerLegHeight leg height} for connection and UI testing if
      * {@link #realModeSwitch real mode} is activated. Status messages are shown, if real mode is
      * turned off or the publisher is not available.
      * <p>This message is otherwise generated by ROS whenever the leg is moving.</p>
      *
      * @see OperatorTerminalNode#AthlasLegHeightPublisher AthlasLegHeightPublisher
      * @see #legSeeker
      */
     private void testAthlasLegHeight() {
         if(operatorTerminalNode.isReal()){
             if(operatorTerminalNode.AthlasLegHeightPublisher != null) {
                 AthlasLegHeight legHeightMsg = operatorTerminalNode.messageFactory.newFromType(AthlasLegHeight._TYPE);
                 legHeightMsg.setLegNumber((short) legTestNr);
                 legHeightMsg.setHeight(legSeekerLegHeight);
                 Log.i(TAG, "testing leg operation to: " + legSeekerLegHeight);
                 operatorTerminalNode.AthlasLegHeightPublisher.publish(legHeightMsg);
             } else {
                 mainActivity.textStatusFragment.pushStatus("LEG PUB NOT AVAIL");
             }
         } else {
             mainActivity.textStatusFragment.pushStatusRealMode(false);
             Log.i(TAG, "testing non-real leg operation to: " + legSeekerLegHeight);
             operatorTerminalNode.setLegHeight(legSeekerLegHeight, legTestNr);
         }
         updateLegHeightTV();
     }

     /**
      * Publishes an {@code AthlasEarthHeight} message with the selected {@link #legTestNr leg number}
      * and {@link #earthSeekerEarthHeight earth height} for connection and UI testing if
      * {@link #realModeSwitch real mode} is activated. Status messages are shown, if real mode is
      * turned off or the publisher is not available.
      * <p>This message is otherwise generated by ROS whenever the measured distance between foot
      * and ground changes.</p>
      *
      * @see OperatorTerminalNode#AthlasEarthHeightPublisher AthlasEarthHeightPublisher
      * @see #earthSeeker
      */
     private void testAthlasEarthHeight(){
         if(operatorTerminalNode.isReal()){
             if(operatorTerminalNode.AthlasEarthHeightPublisher != null) {
                 AthlasEarthHeight earthHeightMsg = operatorTerminalNode.messageFactory.newFromType(AthlasEarthHeight._TYPE);
                 earthHeightMsg.setLegNumber((short) legTestNr);
                 earthHeightMsg.setHeight(earthSeekerEarthHeight);
                 operatorTerminalNode.AthlasEarthHeightPublisher.publish(earthHeightMsg);
             } else {
                 mainActivity.textStatusFragment.pushStatus("EARTH PUB NOT AVAIL");
             }
         } else {
             mainActivity.textStatusFragment.pushStatusRealMode(false);
             operatorTerminalNode.setEarthHeight(earthSeekerEarthHeight, legTestNr);
         }
     }

     /**
      * Publishes an {@code AthlasLegForce} message with the selected {@link #legTestNr leg number}
      * and {@link #forceSeekerForceLevel leg force} for connection and UI testing if
      * {@link #realModeSwitch real mode} is activated. Status messages are shown, if real mode is
      * turned off or the publisher is not available.
      * <p>This message is otherwise generated by ROS whenever the measured leg force changes.</p>
      *
      * @see OperatorTerminalNode#AthlasLegForcePublisher AthlasLegForcePublisher
      * @see #forceSeeker
      */
     private void testAthlasLegForce(){
        if(operatorTerminalNode.isReal()){
            if(operatorTerminalNode.AthlasLegForcePublisher != null){
                AthlasLegForce legForceMsg = operatorTerminalNode.messageFactory.newFromType(AthlasLegForce._TYPE);
                legForceMsg.setLegNumber((short) legTestNr);
                legForceMsg.setForce(forceSeekerForceLevel);
                operatorTerminalNode.AthlasLegForcePublisher.publish(legForceMsg);
            } else {
                mainActivity.textStatusFragment.pushStatus("FORCE PUB NOT AVAIL");
            }
        } else {
            mainActivity.textStatusFragment.pushStatusRealMode(false);
            operatorTerminalNode.setLegForce(forceSeekerForceLevel, legTestNr);
        }
        updateMotorCurrentTV();
     }

     /**
      * Displays the stored raw leg position value of the selected {@link #legTestNr leg number}.
      *
      * @see OperatorTerminalNode#athlasLegs
      * @see AthlasLeg#legHeight
      */
     public void updateLegHeightTV(){
         updateLegLimitTv();

         final String legHeightText = "Current leg height: ";
         float legHeight = operatorTerminalNode.athlasLegs.get(legTestNr).legHeight;
         double roundedLegHeight = Math.round(legHeight * 1000d) / 1000d;

         final String tvLegHeightText = legHeightText + roundedLegHeight;

         getActivity().runOnUiThread(new Runnable() {
             @Override
             public void run() {
                 tvlegHeight.setText(tvLegHeightText);
             }
         });
     }

     /**
      * Displays {@link #tvLegUpperLimit leg movement limits}.
      */
     public void updateLegLimitTv(){
         tvLegUpperLimit.setText(mainActivity.legUpperLimitS);
         tvLegLowerLimit.setText(mainActivity.legLowerLimitS);
     }

     /**
      * Displays the stored raw leg force/motor current value of the selected {@link #legTestNr leg number}.
      */
     public void updateMotorCurrentTV(){
         final String motorCurrentText = "Motor current/force: ";
         float motorCurrent = operatorTerminalNode.athlasLegs.get(legTestNr).legForce;
         double roundedMotorCurrent = Math.round(motorCurrent * 1000d)/1000d;

         final String tvMotorCurrentText = motorCurrentText + roundedMotorCurrent;

         getActivity().runOnUiThread(new Runnable() {
             @Override
             public void run() {
                 tvMotorCurrent.setText(tvMotorCurrentText);
             }
         });
     }

     /**
      * Find all UI elements and match them to their local variable representations.
      */
     private void initViews(){
         btnLegUp = (Button) rootView.findViewById(R.id.btn_leg_up);
         btnParking = (Button) rootView.findViewById(R.id.btn_leg_parking);
         btnLegHigh = (Button) rootView.findViewById(R.id.btn_leg_high);
         btnLegAuto = (Button) rootView.findViewById(R.id.btn_leg_auto);
         btnLegLow = (Button) rootView.findViewById(R.id.btn_leg_low);
         btnLegEmergency = (Button) rootView.findViewById(R.id.btn_leg_emergency) ;

         btnBrakeStateMsgTest = (Button) rootView.findViewById(R.id.btn_test_OTBrakeStateChangeMsg);
         btnBrakeOperation = (Button) rootView.findViewById(R.id.btn_brake);

         tvLegUpperLimit = (TextView) rootView.findViewById(R.id.tv_leg_upper_limit);
         tvLegLowerLimit = (TextView) rootView.findViewById(R.id.tv_leg_lower_limit);
         tvlegHeight = (TextView) rootView.findViewById(R.id.tv_leg_height);
         tvMotorCurrent = (TextView) rootView.findViewById(R.id.tv_motor_current);

         realModeSwitch = (Switch) rootView.findViewById(R.id.real_mode_switch);
         brakeSwitch = (Switch) rootView.findViewById(R.id.brake_switch);

         legSeeker = (SeekBar) rootView.findViewById(R.id.leg_seeker);
         earthSeeker = (SeekBar) rootView.findViewById(R.id.earth_seeker);
         forceSeeker = (SeekBar) rootView.findViewById(R.id.force_seeker);
     }

     /**
      * Get leg height, earth height and force limits from {@link LegDisplayFragment} and
      * define seeker action: {@link #testAthlasLegHeight()}, {@link #testAthlasEarthHeight()} and
      * {@link #testAthlasLegForce()}
      */
     private void setUpSeekers(){
         final float legLowerLimit = mainActivity.legLowerLimitF,
                 legUpperLimit = mainActivity.legUpperLimitF;
         legSeeker.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
             @Override
             public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                 legSeekerLegHeight = legUpperLimit + (progress/100.0f)*(legLowerLimit - legUpperLimit);
             }

             @Override
             public void onStartTrackingTouch(SeekBar seekBar) { }

             @Override
             public void onStopTrackingTouch(SeekBar seekBar) {
                 testAthlasLegHeight();
             }
         });
         legSeeker.setProgress(80, true);

         final float earthUpperLimit = mainActivity.earthUpperLimitF,
                 earthLowerLimit = mainActivity.earthLowerLimitF;
         earthSeeker.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
             @Override
             public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                 earthSeekerEarthHeight = earthLowerLimit + (progress/100.0f)*(earthUpperLimit - earthLowerLimit);
             }

             @Override
             public void onStartTrackingTouch(SeekBar seekBar) { }

             @Override
             public void onStopTrackingTouch(SeekBar seekBar) {
                 testAthlasEarthHeight();
             }
         });
         earthSeeker.setProgress(80, true);

         final float forceUpperLimit =  mainActivity.legForceUpperLimit,
                 forceLowerLimit = mainActivity.legForceLowerLimit;
         forceSeeker.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
             @Override
             public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                 forceSeekerForceLevel = forceLowerLimit + (progress/100.0f)*(forceUpperLimit - forceLowerLimit);
             }

             @Override
             public void onStartTrackingTouch(SeekBar seekBar) { }

             @Override
             public void onStopTrackingTouch(SeekBar seekBar) {
                 testAthlasLegForce();
             }
         });


     }

     /**
      * Define {@link #realModeSwitch} error/status display action on the
      * {@link TextStatusFragment text status fragment}:
      * <ul>
      *     <li>ROS CNCTD</li>
      *     <li>ROS NOT CNCTD</li>
      *     <li>REAL M ACTIV</li>
      *     <li>REAL M NOT ACTIV</li>
      * </ul>
      */
     private void setUpSwitches(){
         realModeSwitch.setChecked(false);
         brakeSwitch.setChecked(false);

         realModeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
             @Override
             public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                 operatorTerminalNode.toggleRealMode(isChecked);
                 mainActivity.textStatusFragment.pushMessageROSConnection();
             }
         });
     }

     /**
      * Relays leg command {@link #btnLegUp button clicks} to the leg operation
      * {@link OperatorTerminalNode#operateLeg(AthlasLeg.LegCmd, int) node function}.
      */
     private class LegButtonListener implements View.OnClickListener{
         @Override
         public void onClick(View v) {
             AthlasLeg.LegCmd legCmd = AthlasLeg.LegCmd.Emergency;
             switch(v.getId()){
                 case R.id.btn_leg_up:
                     legCmd = AthlasLeg.LegCmd.Up;
                     break;
                 case R.id.btn_leg_parking:
                     legCmd = AthlasLeg.LegCmd.Parking;
                     break;
                 case R.id.btn_leg_high:
                     legCmd = AthlasLeg.LegCmd.High;
                     break;
                 case R.id.btn_leg_auto:
                     legCmd = AthlasLeg.LegCmd.Auto;
                     break;
                 case R.id.btn_leg_low:
                     legCmd = AthlasLeg.LegCmd.Low;
                     break;
                 case R.id.btn_leg_emergency:
                     legCmd = AthlasLeg.LegCmd.Emergency;
                     break;
             }
             operatorTerminalNode.operateLeg(legCmd, legTestNr);
         }
     }

     /**
      * Sets a {@link LegButtonListener LegButtonListener} on every {@link #btnLegUp leg command button}.
      * Link brake test buttons with their action functions.
      *
      * @see #testOTBrakeStateChangeMsg()
      * @see #testBrakeOperation()
      */
     private void setUpButtons(){
         LegButtonListener legButtonListener = new LegButtonListener();
         btnLegUp.setOnClickListener(legButtonListener);
         btnParking.setOnClickListener(legButtonListener);
         btnLegHigh.setOnClickListener(legButtonListener);
         btnLegAuto.setOnClickListener(legButtonListener);
         btnLegLow.setOnClickListener(legButtonListener);
         btnLegEmergency.setOnClickListener(legButtonListener);

         btnBrakeStateMsgTest.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 testOTBrakeStateChangeMsg();
             }
         });

         btnBrakeOperation.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 testBrakeOperation();
             }
         });
     }

     /**
      * Configures the {@link #legNrPicker}.
      */
     private void setUpLegNrPicker(){
        //set up leg-chooser
        legNrPicker = (NumberPicker) rootView.findViewById(R.id.leg_nr_picker);
        legNrPicker.setMaxValue(4);
        legNrPicker.setMinValue(0);
        legNrPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                legTestNr = newVal;
                updateLegHeightTV();
                updateMotorCurrentTV();
            }
        });
        legNrPicker.setSelected(false);
        legNrPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
         legNrPicker.setDisplayedValues(new String[]{"ALL", "1", "2", "3", "4"});


        try {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.title_OT);
        } catch (NullPointerException e) {
            Log.e(TAG, "no Toolbar available");
        }
    }


 }
