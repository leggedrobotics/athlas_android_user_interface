package com.ATHLAS.operator_terminal.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ATHLAS.operator_terminal.R;
import com.ATHLAS.operator_terminal.ros.nodes.OperatorTerminalNode;

/**
 * Holds the {@link ControlLeverView} via the XML-file {@code fragment_controls}.
 *
 * @author Luca Vandeventer
 * @author Flurin Schwerzmann
 */

public class ControlLeverFragment extends Fragment {

    View rootView;
    OperatorTerminalNode operatorTerminalNode;

    MainActivity mainActivity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        rootView = inflater.inflate(R.layout.fragment_control_lever, container, false);
        mainActivity = (MainActivity) getActivity();

        operatorTerminalNode = ((MainActivity) getActivity()).operatorTerminalNode;

        return rootView;
    }

    /**
     * Toggles {@link OperatorTerminalNode#toggleRealMode(boolean) real mode} on, if
     * {@link MainApplication#isConnected() ROS connected}.
     */
    @Override
    public void onResume() {
        super.onResume();

        if(mainActivity.mainApplication.isConnected()) operatorTerminalNode.toggleRealMode(true);
    }

    /**
     * Toggles {@link OperatorTerminalNode#toggleRealMode(boolean) real mode} off, if
     * {@link MainApplication#isConnected() ROS connected}.
     */
    @Override
    public void onPause() {
        super.onPause();

        if(mainActivity.mainApplication.isConnected()) operatorTerminalNode.toggleRealMode(false);
    }
}
