package com.ATHLAS.operator_terminal.ui;

/**
 * Representation of a leg of the ATHLAS system, storing its relevant data.
 *
 * @author Flurin Schwerzmann
 */
public class AthlasLeg {
    /**
     * <b>{@code Up}</b>: Leg stowed for cruise flight <br>
     * <b>{@code Parking}</b>: Highest position without helicopter fuselage ground contact <br>
     * <b>{@code High}</b>: Highest landable position <br>
     * <b>{@code Auto}</b>: ATHLAS leg height controller for landing <br>
     * <b>{@code Low}</b>: Lowest landable position <br>
     * <b>{@code Stop}</b>: Immediately stop movement <br>
     * <b>{@code Emergency}</b>: Trigger emergency action
     */
    public enum LegCmd {Up, Parking, High, Auto, Low, Stop, Emergency}

    /**
     * <b>{@code Moving}</b>: Leg on the move <br>
     * <b>{@code Locked}</b>: Leg confined <br>
     * <b>{@code Error}</b>: Leg confused
     */
    enum LegState {Moving, Locked, Error}

    /**
     * <b>{@code Close}</b>: Brake is set, no leg movement possible <br>
     * <b>{@code Open}</b>: Brake is released, leg movement possible
     */
    public enum BrakeState {Close, Open}

    /**
     * <b>{@code Normal}</b>: leg force/motor current in normal range <br>
     * <b>{@code Warning}</b>: leg force/motor current is higher than normal <br>
     * <b>{@code Overload}</b>: leg force/motor current is too high <br>
     * <b>{@code Error}</b>: standard value, no viable value from ROS available
     */
    public enum LegForceState {Normal, Warning, Overload, Error}

    /**
     * The number of the leg (currently 1-4).
     */
    public final int legNr;
    /**
     * Raw measured value of leg height (<i>not</i> percentage!) <br>
     */
    public float legHeight;
    /**
     * Raw measured value of distance between foot and ground (<i>not</i> percentage!) <br>
     */
    public float earthHeight;
    /**
     * Raw measured value of motor current/leg force (<i>not</i> percentage!)
     */
    public float legForce;

    /**
     * @see BrakeState
     */
    public BrakeState brakeState;

    /**
     * @see LegForceState
     */
    public LegForceState legForceState;

    /**
     * Constructs a new {@code AthlasLeg}. {@code legForceState} is set to {@code Error} by default.
     * @param legNr Number of the leg
     * @param legHeight Raw  leg height value
     * @param earthHeight Raw earth height value
     * @param legForce Raw leg force/motor current value
     * @param brakeState State of the brake
     */
    public AthlasLeg(int legNr, float legHeight, float earthHeight, float legForce, BrakeState brakeState) {
        this.legNr = legNr;
        this.legHeight = legHeight;
        this.earthHeight = earthHeight;
        this.legForce = legForce;
        this.brakeState = brakeState;

        this.legForceState = LegForceState.Error;
    }
}
