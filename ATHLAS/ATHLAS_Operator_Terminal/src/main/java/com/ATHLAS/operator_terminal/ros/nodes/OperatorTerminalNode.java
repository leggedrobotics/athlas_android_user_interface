package com.ATHLAS.operator_terminal.ros.nodes;

import android.content.SharedPreferences;
import android.util.Log;

import com.ATHLAS.operator_terminal.ui.AthlasLeg;
import com.ATHLAS.operator_terminal.ui.MainActivity;
import com.ATHLAS.operator_terminal.ui.TestFragment;

import org.ros.exception.RemoteException;
import org.ros.exception.ServiceException;
import org.ros.exception.ServiceNotFoundException;
import org.ros.message.MessageFactory;
import org.ros.message.MessageListener;
import org.ros.node.ConnectedNode;
import org.ros.node.service.ServiceClient;
import org.ros.node.service.ServiceResponseBuilder;
import org.ros.node.service.ServiceResponseListener;
import org.ros.node.service.ServiceServer;
import org.ros.node.topic.Publisher;
import org.ros.node.topic.Subscriber;

import java.util.ArrayList;

import athlas_msgs.AthlasEarthHeight;
import athlas_msgs.AthlasLegForce;
import athlas_msgs.AthlasLegHeight;
import athlas_msgs.GetLegHeight;
import athlas_msgs.GetLegHeightRequest;
import athlas_msgs.GetLegHeightResponse;
import athlas_msgs.OTBrakeStateChangeMsg;
import athlas_msgs.OTBrakeStateChangeSrv;
import athlas_msgs.OTBrakeStateChangeSrvRequest;
import athlas_msgs.OTBrakeStateChangeSrvResponse;
import athlas_msgs.SetBrakeState;
import athlas_msgs.SetBrakeStateRequest;
import athlas_msgs.SetBrakeStateResponse;
import athlas_msgs.SetEmergencyStop;
import athlas_msgs.SetEmergencyStopRequest;
import athlas_msgs.SetEmergencyStopResponse;
import athlas_msgs.float_service;
import athlas_msgs.float_serviceRequest;
import athlas_msgs.float_serviceResponse;
import std_msgs.Float32;
import std_srvs.Trigger;
import std_srvs.TriggerRequest;
import std_srvs.TriggerResponse;

/**
 * Main ROS-node for the ATHLAS Operator Terminal App. Manages data and commands from the displays
 * to the ROS master and vice versa:
 * <ul>
 *     <li>{@link com.ATHLAS.operator_terminal.ui.BatteryFragment Battery}</li>
 *     <li>{@link com.ATHLAS.operator_terminal.ui.LegDisplayFragment Legs}</li>
 *     <li>{@link TestFragment Test}</li>
 *     <li>{@link com.ATHLAS.operator_terminal.ui.TextStatusFragment Text Status}</li>
 * </ul>
 * Stores the state information of all {@link #athlasLegs AthlasLegs}.
 *
 * @author Flurin Schwerzmann
 */
public class OperatorTerminalNode extends CustomNode {
    /**
     * Tests the brake state visualization
     * on the {@link com.ATHLAS.operator_terminal.ui.LegDisplayFragment leg display}
     * using ROS.
     */
    public Publisher<athlas_msgs.OTBrakeStateChangeMsg> OTBrakeStateChangePublisher;

    /**
     * Tests the leg height visualization
     * on the {@link com.ATHLAS.operator_terminal.ui.LegDisplayFragment leg display}
     * using ROS.
     */
    public Publisher<athlas_msgs.AthlasLegHeight> AthlasLegHeightPublisher;

    /**
     * Tests the earth height visualization
     * on the {@link com.ATHLAS.operator_terminal.ui.LegDisplayFragment leg display}
     * using ROS.
     */
    public Publisher<athlas_msgs.AthlasEarthHeight> AthlasEarthHeightPublisher;

    /**
     * Tests the leg force visualization
     * on the {@link com.ATHLAS.operator_terminal.ui.LegDisplayFragment leg display}
     * using ROS.
     */
    public Publisher<athlas_msgs.AthlasLegForce> AthlasLegForcePublisher;

    /**
     * Notified, when a leg's height has changed. Updates {@code athlasLegs} and
     * {@link com.ATHLAS.operator_terminal.ui.LegDisplayFragment leg display}.
     */
    @SuppressWarnings("FieldCanBeLocal")
    private Subscriber<athlas_msgs.AthlasLegHeight> updateLegHeightSubscriber;

    /**
     * Notified, when a leg's motor current has changed. Motor current is proportional to leg force,
     * its value is stored in {@code athlasLegs}.
     * Limits are checked in
     * {@link com.ATHLAS.operator_terminal.ui.LegDisplayFragment#showLegsForce(ArrayList)
     * LegDisplayFragment.showLegsForce}.
     */
    @SuppressWarnings("FieldCanBeLocal")
    private Subscriber<athlas_msgs.AthlasLegForce> updateMotorCurrentSubscriber;

    /**
     * Notified, when a leg's distance between foot and ground has changed. Value is stored in
     * {@code athlasLegs} and the
     * {@link com.ATHLAS.operator_terminal.ui.LegDisplayFragment leg display} is updated.
     */
    @SuppressWarnings("FieldCanBeLocal")
    private Subscriber<athlas_msgs.AthlasEarthHeight> updateEarthHeightSubscriber; //subscribe to earth height updates (ROS implementation TBD)

    /**
     * Notified, when the battery voltage has changed. Value is stored in the
     * {@link com.ATHLAS.operator_terminal.ui.BatteryFragment battery fragment}.
     */
    @SuppressWarnings("FieldCanBeLocal")
    private Subscriber<std_msgs.Float32> updateBatteryVoltage; //subscribe to battery voltage changes

    /**
     * Requests the current height of a leg from ROS in {@link #syncLegHeight(int)}.
     */
    private ServiceClient<athlas_msgs.GetLegHeightRequest, athlas_msgs.GetLegHeightResponse> getLegHeightService;

    /**
     * Requests leg movement to a certain height in {@link #operateLegToPosition(int, float)}.
     */
    private ServiceClient<athlas_msgs.float_serviceRequest, athlas_msgs.float_serviceResponse> setLegHeightService;

    /**
     * Requests brake operation, used in {@link #operateBrakeTo(AthlasLeg.BrakeState, int)}.
     */
    private ServiceClient<athlas_msgs.SetBrakeStateRequest, athlas_msgs.SetBrakeStateResponse> setBrakeStateService;

    /**
     * Requests movement of legs to the stowed position for cruise. Limits are taken care of by ROS.
     * Used in {@link #operateFlyingMode()}.
     */
    private ServiceClient<std_srvs.TriggerRequest, std_srvs.TriggerResponse> initFlyingModeService;

    /**
     * Requests activation of the ATHLAS control algorithm. Used in {@link #operateLandingMode()}.
     */
    private ServiceClient<std_srvs.TriggerRequest, std_srvs.TriggerResponse> initLandingModeService;

    /**
     * Requests activation of emergency action. Used in {@link #operateEmergencyMode()}.
     */
    private ServiceClient<athlas_msgs.SetEmergencyStopRequest, athlas_msgs.SetEmergencyStopResponse> sendEmergencyService;

    /**
     * Called by the <code>athlas_com</code> node, when the brakes are being operated.
     * Updates {@code athlasLegs} and refreshes the
     * {@link com.ATHLAS.operator_terminal.ui.LegDisplayFragment leg display}.
     * A {@code Service} is needed to make sure, brake operations are always detected by the operator terminal.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private ServiceServer<athlas_msgs.OTBrakeStateChangeSrvRequest, athlas_msgs.OTBrakeStateChangeSrvResponse> OTBrakeStateChangeServer; //listen to BrakeStateChanges

    /**
     * Needed to generate ROS-messages of various types.
     */
    public MessageFactory messageFactory;

    private final String TAG = "OTNode";
    private MainActivity mainActivity;

    /**
     * Status flags for ROS framework, variables and configuration.
     */
    private boolean
            serviceClientsReady = false,
            serversReady = false,
            pubSubReady = false,
            legsReady = false,
            isReal = false;

    /**
     * Stores state information of all four {@link AthlasLeg AthlasLeg}s.
     */
    public ArrayList<AthlasLeg> athlasLegs;

    /**
     * Lean constructor, initializes local {@link MainActivity} reference and
     * {@link #initLegs() legs}.
     */
    public OperatorTerminalNode(String nodeName, MainActivity mainActivity) {
        super(nodeName);
        this.mainActivity = mainActivity;

        initLegs();
    }

    /**
     * Initialize all {@code ServiceClients}, {@code ServiceServers}, {@code Publishers} and {@code Subscribers}.
     * Afterwards, {@link #syncAllLegsWithROS()} updates values.
     *
     * @see #initServiceClients()
     * @see #initServiceServers()
     * @see #initPubSubs()
     */
    @Override
    public void onStart(ConnectedNode connectedNode) {
        super.onStart(connectedNode);

        Log.d(TAG, "onStart");

        serviceClientsReady = initServiceClients();
        serversReady = initServiceServers();
        pubSubReady = initPubSubs();

        syncAllLegsWithROS();

        //tell textStatusFragment, that node has started to update Status/Error messages
        mainActivity.textStatusFragment.onNodeStarted(isNodeReady());
    }

    /**
     * Fill {@link #athlasLegs} with empty standard {@link AthlasLeg}. <br>
     * <b>Warning:</b> hardcoded default values are used <i>not</i> {@code SharedPreferences}!
     */
    private void initLegs(){
        athlasLegs = new ArrayList<AthlasLeg>();

        try {
            int legNr;
            float legHeight = mainActivity.legUpperLimitF,
                    earthHeight = mainActivity.earthLowerLimitF,
                    legForce = mainActivity.legForceLowerLimit;
            AthlasLeg.BrakeState brakeState = AthlasLeg.BrakeState.Close;

            for (int i = 0; i <= 4; i++) {
                legNr = i;
                athlasLegs.add(new AthlasLeg(legNr, legHeight, earthHeight, legForce, brakeState));
            }
        } catch (Exception e){
            Log.e(TAG, "error initializing leg objects");
            legsReady = false;
        }

        legsReady = true;
    }

    /**
     * Connect all {@code ServiceClients} to ROS. <br>
     * <b>Warning:</b> No errors are displayed!
     *
     * @return true, if all {@code ServiceClients} are ready for operation <br>
     *     false, if at least one {@code ServiceClient} has failed to connect
     * @see #getLegHeightService
     * @see #setLegHeightService
     * @see #setBrakeStateService
     * @see #initLandingModeService
     * @see #initFlyingModeService
     * @see #sendEmergencyService
     */
    private boolean initServiceClients(){
        try {
            //set up setLegHeight-ServiceClient
            boolean getLegHeightReady = false;
            try {
                getLegHeightService = nodeMain.newServiceClient(RosConfig.GET_LEG_HEIGHT_SERVICE, GetLegHeight._TYPE);
                if (getLegHeightService.isConnected()) {
                    Log.i(TAG, "GetLegHeightService is connected");
                    getLegHeightReady = true;
                } else Log.i(TAG, "GetLegHeightService is NOT conntected");
            } catch (ServiceNotFoundException e) {
                Log.e(TAG, "GetLegHeightService not found: \n" + e.fillInStackTrace());
            }

            //set up setLegHeight-ServiceClient
            boolean setLegHeightReady = false;
            try {
                setLegHeightService = nodeMain.newServiceClient(RosConfig.SET_LEG_HEIGHT_SERVICE, float_service._TYPE);
                if (setLegHeightService.isConnected()) {
                    Log.i(TAG, "setLegHeightService is connected");
                    setLegHeightReady = true;
                } else Log.i(TAG, "setLegHeightService is NOT connected");
            } catch (ServiceNotFoundException e) {
                Log.e(TAG, "SetLegHeightService not found: \n" + e.fillInStackTrace());
            }

            //set up setBrakeState-ServiceClient
            boolean setBrakeStateReady = false;
            try {
                setBrakeStateService = nodeMain.newServiceClient(RosConfig.SET_BRAKE_STATE_SERVICE, SetBrakeState._TYPE);
                if (setBrakeStateService.isConnected()) {
                    Log.i(TAG, "setBrakeStateService is connected");
                    setBrakeStateReady = true;
                } else Log.i(TAG, "setBrakeStateService is NOT connected");
            } catch (ServiceNotFoundException e) {
                Log.e(TAG, "SetBrakeStateService not found: \n" + e.fillInStackTrace());
            }

            //set up initFlying-ServiceClient
            boolean initFlyingReady = false;
            try {
                initFlyingModeService = nodeMain.newServiceClient(RosConfig.INIT_FLYING_MODE_SERVICE, Trigger._TYPE);
                if (initFlyingModeService.isConnected()) {
                    Log.i(TAG, "initFlyingModeService is connected");
                    initFlyingReady = true;
                } else Log.i(TAG, "initFlyingModeService is NOT connected");
            } catch (ServiceNotFoundException e) {
                Log.e(TAG, "InitFlyingModeService not found: \n" + e.fillInStackTrace());
            }

            //set up initLanding-ServiceClient
            boolean initLandingReady = false;
            try {
                initLandingModeService = nodeMain.newServiceClient(RosConfig.INIT_LANDING_MODE_SERVICE, Trigger._TYPE);
                if (initLandingModeService.isConnected()) {
                    Log.i(TAG, "initLandingModeService is connected");
                    initLandingReady = true;
                } else Log.i(TAG, "initLandingModeService is NOT connected");
            } catch (ServiceNotFoundException e) {
                Log.e(TAG, "InitLandingModeService not found: \n" + e.fillInStackTrace());
            }

            //set up sendEmergency-ServiceClient
            boolean sendEmergencyReady = false;
            try {
                sendEmergencyService = nodeMain.newServiceClient(RosConfig.SEND_EMERGENCY_SERVICE, SetEmergencyStop._TYPE);
                if (sendEmergencyService.isConnected()) {
                    Log.i(TAG, "sendEmergencyService is connected");
                    sendEmergencyReady = true;
                } else {
                    Log.i(TAG, "sendEmergencyService is NOT connected");
                    mainActivity.textStatusFragment.pushError("EMER SRV NOT CNCTD");
                }
            } catch (ServiceNotFoundException e) {
                Log.e(TAG, "SendEmergencyService not found: \n" + e.fillInStackTrace());
                mainActivity.textStatusFragment.pushError("EMER SRV NOT AVAIL");
            }

            return (getLegHeightReady && setLegHeightReady && setBrakeStateReady && initFlyingReady && initLandingReady && sendEmergencyReady);

        } catch (Exception e){
            Log.e(TAG, "Error setting up Services: \n" + e.fillInStackTrace());
            mainActivity.textStatusFragment.pushError("SRV ERR");
            return false;
        }

    }

    /**
     * Connect and set up the {@link #OTBrakeStateChangeServer} with ROS.
     * @return true, if {@code OTBrakeStateChangeServer} is ready to operate <br>
     *     false, if there was an error during set up
     */
    private boolean initServiceServers(){
        //set up BrakeStateChange-Server
        try {
            OTBrakeStateChangeServer = nodeMain.newServiceServer(RosConfig.OT_BRAKE_STATE_SERVICE, OTBrakeStateChangeSrv._TYPE,
                new ServiceResponseBuilder<OTBrakeStateChangeSrvRequest, OTBrakeStateChangeSrvResponse>() {
                    @Override
                    public void build(OTBrakeStateChangeSrvRequest otBrakeStateChangeSrvRequest, OTBrakeStateChangeSrvResponse otBrakeStateChangeSrvResponse) throws ServiceException {
                        boolean isBrakeSet = otBrakeStateChangeSrvRequest.getIsBrakeSet();
                        int legNr = otBrakeStateChangeSrvRequest.getLegNumber();

                        String msg = "OTBrakeStateChangeServer Server called! \n leg nr: " + String.valueOf(legNr) + "\n isBrakeSet: " + String.valueOf(isBrakeSet);
                        Log.i(TAG, msg);

                        AthlasLeg.BrakeState brakeState;
                        if(isBrakeSet) brakeState = AthlasLeg.BrakeState.Close;
                        else brakeState = AthlasLeg.BrakeState.Open;

                        setLegBrakeState(brakeState, legNr);

                        otBrakeStateChangeSrvResponse.setMessageReceived(true);
                    }
                });
        } catch(Exception e){
            Log.e(TAG, "Setting up OTBrakeStateChangeServer failed: \n" + e.fillInStackTrace());
            return false;
        }

        return true;
    }

    /**
     * Connect and set up all {@code Publishers} and {@code Subscribers}.
     *
     * @return true, if all {@code Subscribers} are ready <br>
     *     false, if at least one {@code Subscriber} failed to initialize
     * @see #OTBrakeStateChangePublisher
     * @see #AthlasLegHeightPublisher
     * @see #AthlasEarthHeightPublisher
     * @see #AthlasLegForcePublisher
     * @see #updateLegHeightSubscriber
     * @see #updateMotorCurrentSubscriber
     * @see #updateEarthHeightSubscriber
     * @see #updateBatteryVoltage
     */
    private boolean initPubSubs(){
//        PUBLISHER = nodeMain.newPublisher(RosConfig.TOPIC-STRING_FROM_YAML, MESSAGE._TYPE);
//        legHeightPublisher = nodeMain.newPublisher(RosConfig.SET_LEG_HEIGHT_TOPIC, AthlasLegHeight._TYPE);
        OTBrakeStateChangePublisher = nodeMain.newPublisher(RosConfig.OT_BRAKE_STATE_TOPIC, OTBrakeStateChangeMsg._TYPE);
        AthlasLegHeightPublisher = nodeMain.newPublisher(RosConfig.UPDATE_LEG_HEIGHT_TOPIC, AthlasLegHeight._TYPE);
        AthlasEarthHeightPublisher = nodeMain.newPublisher(RosConfig.UPDATE_EARTH_HEIGHT_TOPIC, AthlasEarthHeight._TYPE);
        AthlasLegForcePublisher = nodeMain.newPublisher(RosConfig.UPDATE_LEG_FORCE_TOPIC, AthlasLegForce._TYPE);
        messageFactory = nodeMain.getTopicMessageFactory();

        updateLegHeightSubscriber = nodeMain.newSubscriber(RosConfig.UPDATE_LEG_HEIGHT_TOPIC, AthlasLegHeight._TYPE);
        updateMotorCurrentSubscriber = nodeMain.newSubscriber(RosConfig.UPDATE_LEG_FORCE_TOPIC, AthlasLegForce._TYPE);
        updateEarthHeightSubscriber = nodeMain.newSubscriber(RosConfig.UPDATE_EARTH_HEIGHT_TOPIC, AthlasEarthHeight._TYPE);
        updateBatteryVoltage = nodeMain.newSubscriber(RosConfig.UPDATE_BATTERY_VOLTAGE, Float32._TYPE);

        boolean legHeightSubReady = false;
        try {
            updateLegHeightSubscriber.addMessageListener(new MessageListener<AthlasLegHeight>() {
                @Override
                public void onNewMessage(AthlasLegHeight athlasLegHeight) {
                    int legNr = athlasLegHeight.getLegNumber();
                    float legHeight = athlasLegHeight.getHeight();
                    setLegHeight(legHeight, legNr);
                }
            });
            legHeightSubReady = true;
        } catch(Exception e) {
            Log.e(TAG, "Error setting up LegHeightSubscriber: \n" + e.fillInStackTrace());
        }

        boolean motorCurrentSubReady = false;
        try {
            updateMotorCurrentSubscriber.addMessageListener(new MessageListener<AthlasLegForce>() {
                @Override
                public void onNewMessage(AthlasLegForce athlasLegForce) {
                    int legNr = athlasLegForce.getLegNumber();
                    float legForce = athlasLegForce.getForce();
                    setLegForce(legForce, legNr);
                }
            });
            motorCurrentSubReady = true;
        } catch(Exception e){
            Log.e(TAG, "Error setting up MotorCurrentSubscriber: \n" + e.fillInStackTrace());
        }

        boolean earthHeightSubReady = false;
        try{
            updateEarthHeightSubscriber.addMessageListener(new MessageListener<AthlasEarthHeight>() {
                @Override
                public void onNewMessage(AthlasEarthHeight athlasEarthHeight) {
                    int legNr = athlasEarthHeight.getLegNumber();
                    float earthHeight = athlasEarthHeight.getHeight();
                    setEarthHeight(earthHeight, legNr);
                }
            });
            earthHeightSubReady = true;
        } catch(Exception e){
            Log.e(TAG, "Error setting up EarthHeightSubscriber: \n" + e.fillInStackTrace());
        }

        boolean batterySubReady = false;
        try{
            updateBatteryVoltage.addMessageListener(new MessageListener<Float32>() {
                @Override
                public void onNewMessage(Float32 float32) {
                    mainActivity.batteryFragment.updateBatteryVoltage(float32.getData());
                }
            });
            batterySubReady = true;
        } catch(Exception e){
            Log.e(TAG, "Error setting up BatteryVoltageSubscriber: \n" + e.fillInStackTrace());
        }

        return (legHeightSubReady && motorCurrentSubReady && earthHeightSubReady && batterySubReady);
    }

    /**
     * @return true, if {@code ServiceClients}, {@code ServiceServers}, {@code Subscribers},
     * {@code Publishers} and {@code athlasLegs} are all operational <br>
     *     false, if a single component failed to initialize
     */
    public boolean isNodeReady(){
        return serviceClientsReady && serversReady && pubSubReady && legsReady;
    }

    /**
     * Calls {@link #syncLegHeight(int)}, {@link #syncBrakeState(int)}, {@link #syncEarthHeight(int)}
     * and {@link #syncLegForce(int)} for each {@link AthlasLeg} in {@link #athlasLegs}.
     */
    private void syncAllLegsWithROS(){
        if(isNodeReady() && legsReady) {
            for (AthlasLeg leg : athlasLegs) {
                syncLegHeight(leg.legNr);
                syncBrakeState(leg.legNr); //currently INOP
                syncEarthHeight(leg.legNr); //currently INOP
                syncLegForce(leg.legNr); //currently INOP
            }
        }
    }

    /**
     * Sets the {@code legHeight} of one or every {@link AthlasLeg} in {@link #athlasLegs}.
     * The {@link com.ATHLAS.operator_terminal.ui.LegDisplayFragment leg display} is updated.
     * @param legHeight The {@link AthlasLeg#legHeight leg height} to be set
     * @param legNr The {@link AthlasLeg#legNr leg number} of the leg, whose leg height needs to be
     *              changed, 0: every leg, 1-4: single leg
     */
    public void setLegHeight(float legHeight, int legNr){
        if(legsReady) {
            switch (legNr) {
                case 0: //all legs
                    for (AthlasLeg leg : athlasLegs) {
                        leg.legHeight = legHeight;
                    }
                    break;
                default: //single leg
                    athlasLegs.get(legNr).legHeight = legHeight;
                    break;
            }

                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                public void run() {
                    mainActivity.legDisplayFragment.showLegsHeight(athlasLegs);
                }
            });
        }
    }

    /**
     * Sets the {@code earthHeight} of every {@link AthlasLeg} in {@link #athlasLegs}.
     * The {@link com.ATHLAS.operator_terminal.ui.LegDisplayFragment leg display} is updated.
     * @param earthHeight The {@link AthlasLeg#earthHeight earth height} to be set
     * @param legNr The {@link AthlasLeg#legNr leg number} of the leg, whose earth height needs to be
     *              changed, 0: every leg, 1-4: single leg
     */
    public void setEarthHeight(float earthHeight, int legNr){
        if(legsReady) {
            switch (legNr){
                case 0: //all legs
                    for (AthlasLeg leg : athlasLegs) {
                        leg.earthHeight = earthHeight;
                    }
                    break;
                default: //single leg
                    athlasLegs.get(legNr).earthHeight = earthHeight;
            }

            mainActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mainActivity.legDisplayFragment.showEarthHeight(athlasLegs);
                }
            });
        }
    }

    /**
     * Sets the {@code legsForce} of one or every {@link AthlasLeg} in {@link #athlasLegs} and calls
     * {@link #checkLegForce(AthlasLeg)}.
     * The {@link com.ATHLAS.operator_terminal.ui.LegDisplayFragment leg display} is updated.
     * @param legForce The {@link AthlasLeg#legForce leg force} to be set
     * @param legNr The {@link AthlasLeg#legNr leg number} of the leg, whose force value needs to be
     *              changed, 0: every leg, 1-4: single leg
     */
    public void setLegForce(float legForce, int legNr){
        if(legsReady) {
            switch (legNr) {
                case 0: // all legs
                    for (AthlasLeg leg : athlasLegs) {
                        leg.legForce = legForce;
                        checkLegForce(leg);
                    }
                    break;
                default: //single leg
                    athlasLegs.get(legNr).legForce = legForce;
                    checkLegForce(athlasLegs.get(legNr));
            }

            mainActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mainActivity.legDisplayFragment.showLegsForce(athlasLegs);
                }
            });
        }
    }

    /**
     * Sets the {@code brakeState} of one or every {@link AthlasLeg} in {@link #athlasLegs}.
     * The {@link com.ATHLAS.operator_terminal.ui.LegDisplayFragment leg display} is updated.
     * @param brakeState The {@link com.ATHLAS.operator_terminal.ui.AthlasLeg.BrakeState brake state}
     *                   to be set
     * @param legNr The {@link AthlasLeg#legNr leg number} of the leg, whose brake state needs to be
     *              changed, 0: every leg, 1-4: single leg
     */
    private void setLegBrakeState(AthlasLeg.BrakeState brakeState, int legNr){
        if(legsReady) {
            switch(legNr){
                case 0: //all legs
                    for (AthlasLeg leg : athlasLegs) {
                        leg.brakeState = brakeState;
                    }
                    break;
                default: //single leg
                    athlasLegs.get(legNr).brakeState = brakeState;
            }


            mainActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mainActivity.legDisplayFragment.showBrakeStates(athlasLegs);
                }
            });
        }
    }

    /**
     * Calls {@link #getLegHeightService} to sync a single {@link AthlasLeg#legHeight leg height}
     * from {@link #athlasLegs}.
     * @param legNr The {@link AthlasLeg#legNr number} of the leg, whose height should be synced.
     * @return true, if sync was successfull <br>
     *     false, if sync failed
     */
    private boolean syncLegHeight(final int legNr){
        if (getLegHeightService != null) {
            GetLegHeightRequest getLegHeightRequest = getLegHeightService.newMessage();
            getLegHeightRequest.setLegNumber((short) legNr);

            getLegHeightService.call(getLegHeightRequest, new ServiceResponseListener<GetLegHeightResponse>() {
                @Override
                public void onSuccess(GetLegHeightResponse getLegHeightResponse) {
                    setLegHeight(getLegHeightResponse.getHeight(), legNr);
                }

                @Override
                public void onFailure(RemoteException e) {
                    Log.e(TAG, "Error calling GetLegHeight-Service: \n" + e.toString());
                }
            });

            return true;
        } else {
            return false;
        }
    }

    //pull current BrakeStates from ROS
    private boolean syncBrakeState(int legNr){
        //TODO: poll BrakeState from ROS
        return false;
    }

    //pull current EartHeights from ROS
    private boolean syncEarthHeight(final int legNr){
        //TODO: poll EarthHeight from ROS
        return false;
    }

    //pull current LegForces from ROS
    private boolean syncLegForce(final int legNr){
        //TODO: poll LegForce from ROS
        return false;
    }

    /**
     * Updates the {@link com.ATHLAS.operator_terminal.ui.AthlasLeg.LegForceState LegForceState}
     * of an {@link AthlasLeg} based on its {@link AthlasLeg#legForce legForce} and the force limits
     * loaded from {@link SharedPreferences} in {@link MainActivity#loadForceLimits()}.
     * @param leg The {@link AthlasLeg} to be force-checked.
     */
    private void checkLegForce(AthlasLeg leg){
        AthlasLeg.LegForceState legForceState = AthlasLeg.LegForceState.Error;

        if(mainActivity.forceLimitsReady) {
            float legForce = leg.legForce;

            if(mainActivity.legForceLowerLimit < legForce && legForce < mainActivity.legForceWarning) legForceState = AthlasLeg.LegForceState.Normal;
            else if(mainActivity.legForceWarning < legForce && legForce < mainActivity.legForceUpperLimit) legForceState = AthlasLeg.LegForceState.Warning;
            else if(mainActivity.legForceUpperLimit < legForce) legForceState = AthlasLeg.LegForceState.Overload;

            leg.legForceState = legForceState;
        }
    }

    /**
     * Switches between real and non-real mode. <br>
     * @param isReal true: ROS connected operation, false: self-contained app usage
     */
    public void toggleRealMode(boolean isReal){
            this.isReal = isReal;
            mainActivity.textStatusFragment.pushStatusRealMode(isReal);
    }

    /**
     * @return true, if real mode is activated <br>
     *     false, otherwise
     */
    public boolean isReal(){
        return this.isReal;
    }

    /**
     * Command leg operation.
     *
     * @param legCmd What action?
     * @param legNr Which leg?
     * @see #operateFlyingMode()
     * @see #operateLegToPosition(int, float)
     * @see #operateLandingMode()
     * @see #operateStopLegMovement(int)
     * @see #operateEmergencyMode()
     */
    public void operateLeg(AthlasLeg.LegCmd legCmd, int legNr){
        switch (legCmd) {
            case Up: //gear up (stowed)
                operateFlyingMode();
                Log.i(TAG, "Leg moving to UP (flying mode)");
                break;
            case Parking://moving leg to highest position without fuselage ground contact
                operateLegToPosition(legNr, mainActivity.legUpperLimitF);
                Log.i(TAG, "Leg movint to PARKING (" + mainActivity.legUpperLimitF + ")");
            case High: //moving leg to highest possible position for landing
                operateLegToPosition(legNr, mainActivity.legUpperLimitF);
                Log.i(TAG, "Leg moving to HIGH (" + mainActivity.legUpperLimitF + ")");
                break;
            case Auto: //activate leg height controller for landing
                operateLandingMode();
                Log.i(TAG, "Leg moving with AUTO (landing mode)");
                break;
            case Low: //moving leg to lowest possible position for landing
                operateLegToPosition(legNr, mainActivity.legLowerLimitF);
                Log.i(TAG, "Leg moving to LOW (" + mainActivity.legLowerLimitF + ")");
                break;
            case Stop:
                operateStopLegMovement(legNr);
                Log.i(TAG, "Leg movement STOP");
                break;
            case Emergency:
                operateEmergencyMode();
                Log.e(TAG, "LEG EMERGENCY TRIGGERED");
        }
    }

    /**
     * Actuates a leg brake.
     *
     * @param brakeCmd Switch brake to this state
     * @param legNr The {@link AthlasLeg#legNr number} of the leg, whose brake should be operated,
     *              0: all legs, 1-4: single leg.
     * @return true, if brake actuation succeded <br>
     *     false, if brake actuation failed
     * @see #setBrakeStateService
     */
    public boolean operateBrakeTo(final AthlasLeg.BrakeState brakeCmd, final int legNr) {
        if (isReal) {
            if (setBrakeStateService != null) {
                final SetBrakeStateRequest setBrakeStateRequest = setBrakeStateService.newMessage();

                switch (brakeCmd) {
                    case Close:
                        setBrakeStateRequest.setBrakeState(true);
                        break;
                    case Open:
                        setBrakeStateRequest.setBrakeState(false);
                        break;
                }

                setBrakeStateRequest.setLegNumber((short) legNr);

                setBrakeStateService.call(setBrakeStateRequest, new ServiceResponseListener<SetBrakeStateResponse>() {
                    @Override
                    public void onSuccess(SetBrakeStateResponse setBrakeStateResponse) {
                        Log.i(TAG, "SetBrakeStateRequest successfully sent: "
                                + String.valueOf(setBrakeStateRequest.getBrakeState()
                                + " leg nr. " + setBrakeStateRequest.getLegNumber()));
                        setLegBrakeState(brakeCmd, legNr); //legNr = 0 (all legs)
                    }

                    @Override
                    public void onFailure(RemoteException e) {
                        Log.i(TAG, "SetBrakeStateRequest failed " + e.getStatusCode());
                        mainActivity.textStatusFragment.pushError("BRK SRV NOT AVAIL");
                    }
                });
                return true;
            } else {
                mainActivity.textStatusFragment.pushError("BRK SRV NOT AVAIL");
                return false;
            }
        } else {
            Log.i(TAG, "Real mode is turned off.");
            setLegBrakeState(brakeCmd, legNr);
            mainActivity.textStatusFragment.pushStatusRealMode(false);
            return false;
        }


    }

    /**
     * Request leg movement to a specific height.
     * @param legNr The {@link AthlasLeg#legNr number} of the leg to be moved.
     * @param legHeight The requested {@link AthlasLeg#legHeight leg height}.
     * @see #setLegHeightService
     */
    private void operateLegToPosition(int legNr, float legHeight) {
        if (setLegHeightService != null) {
            final float_serviceRequest setLegHeightRequest = setLegHeightService.newMessage();
            setLegHeightRequest.setLegNumber((short) legNr);
            setLegHeightRequest.setValue(legHeight);

            if (isReal) {
                Log.i(TAG, "operating leg to position: " + legHeight);
                setLegHeightService.call(setLegHeightRequest, new ServiceResponseListener<float_serviceResponse>() {
                    @Override
                    public void onSuccess(float_serviceResponse setLegHeight) {
                        Log.i(TAG, "setLegHeightRequest successfully sent: " + setLegHeightRequest.getValue());
                    }

                    @Override
                    public void onFailure(RemoteException e) {
                        Log.i(TAG, "setLegHeightRequest failed \n" + e.getStatusCode());
                    }
                });
            } else {
                Log.i(TAG, "real mode is turned off");
                mainActivity.textStatusFragment.pushStatusRealMode(false);
            }
        } else {
            mainActivity.textStatusFragment.pushError("LEG SRV NOT AVAIL");
        }
    }

    /**
     * Request activation of the ATHLAS leg height control algorithm for landing.
     *
     * @see #initLandingModeService
     */
    private void operateLandingMode(){
        if(initLandingModeService != null){
            final TriggerRequest initLandingRequest = initLandingModeService.newMessage();
            if(isReal){
                initLandingModeService.call(initLandingRequest, new ServiceResponseListener<TriggerResponse>() {
                    @Override
                    public void onSuccess(TriggerResponse triggerResponse) {
                        Log.i(TAG, "initLandingRequest successfully sent: " + triggerResponse.getMessage());
                    }

                    @Override
                    public void onFailure(RemoteException e) {
                        Log.i(TAG, "initLandingRequest failed \n" + e.getStatusCode());
                    }
                });
            } else {
                Log.i(TAG, "real mode is turned off");
                mainActivity.textStatusFragment.pushStatusRealMode(false);
            }
        } else {
            mainActivity.textStatusFragment.pushError("LDG M SRV NOT AVAIL");
        }
    }

    /**
     * Requests leg movement to the
     * {@link com.ATHLAS.operator_terminal.ui.AthlasLeg.LegCmd UP}
     * (stowed) position for cruise.
     *
     * @see #initFlyingModeService
     */
    private void operateFlyingMode(){
        if(initFlyingModeService != null){
            final TriggerRequest initFlyingRequest = initFlyingModeService.newMessage();
            if(isReal){
                initFlyingModeService.call(initFlyingRequest, new ServiceResponseListener<TriggerResponse>() {
                    @Override
                    public void onSuccess(TriggerResponse triggerResponse) {
                        Log.i(TAG, "initFlyingRequest successfully sent: " + triggerResponse.getMessage());
                    }

                    @Override
                    public void onFailure(RemoteException e) {
                        Log.i(TAG, "initFlyingRequest failed \n" + e.getStatusCode());
                    }
                });
            } else {
                Log.i(TAG, "real mode is turned off");
                mainActivity.textStatusFragment.pushStatusRealMode(false);
            }
        } else {
            mainActivity.textStatusFragment.pushError("FLY M SRV NOT AVAIL");
        }
    }

    private void operateStopLegMovement(int legNr){
        Log.i(TAG, "operateStopLegMovement called, legNr=" + legNr);
        //TODO: stop leg movement
    }

    /**
     * Request emergency action from ROS.
     * @see #sendEmergencyService
     */
    private void operateEmergencyMode(){
        if(sendEmergencyService != null){
            final SetEmergencyStopRequest emergencyRequest = sendEmergencyService.newMessage();
            emergencyRequest.setStop(true);

            if(isReal){
                sendEmergencyService.call(emergencyRequest, new ServiceResponseListener<SetEmergencyStopResponse>() {
                    @Override
                    public void onSuccess(SetEmergencyStopResponse emergencyResponse) {
                        Log.i(TAG, "Emergency triggered");
                        mainActivity.textStatusFragment.pushStatus("EMER ACTIV");
                    }

                    @Override
                    public void onFailure(RemoteException e) {
                        Log.e(TAG, "Emergency FAILED");
                        mainActivity.textStatusFragment.pushError("EMER FAIL");
                    }
                });
            } else {
                Log.i(TAG, "real mode is turned off");
                mainActivity.textStatusFragment.pushStatusRealMode(false);
            }
        } else {
            mainActivity.textStatusFragment.pushError("EMER SRV NOT AVAIL");

        }
    }

}
