package com.ATHLAS.operator_terminal.ui;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;

/**
 * Displays the battery charge percentage using two drawn {@code Rect} as a progress bar.
 *
 * @author Luca Vandeventer
 */

public class BatteryProgressView extends View {

    /**
     * Helps create custom progress bar <br>
     * <b>progress</b>: rectangle showing actual percentage left in battery <br>
     * <b>legHeight</b>: constant rectangle displaying the size of the progress bar <br>
     */
    private RectF progress,
                progressBackground;

    private Paint progressPaint,
                progressBackgroundPaint;

    /**
     * Creates line, showing exact actual percentage state.
     */
    private Paint whiteLine;

    /**
     * Holds battery percentage. <br>
     * <b> Caution: </b> Should never be lower than 0 or over 100.
     */
    int batteryState = 100;

    int width, height = 0;

    DisplayMetrics displayMetrics;

    public BatteryProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);

        initPaint();
    }

    /**
     *@see LegStatusView#onSizeChanged
     */
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        width = w;
        height = h;

        displayMetrics = getResources().getDisplayMetrics();
        initVars();
    }

    /**
     * Initializes all {@code Paint} objects.
     */
    void initPaint(){
        progressPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        progressPaint.setColor(Color.rgb(0,200,83));

        progressBackgroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        progressBackgroundPaint.setColor(Color.argb(50,0,200,83));

        whiteLine = new Paint(Paint.ANTI_ALIAS_FLAG);
        whiteLine.setColor(Color.WHITE);
    }

    /**
     * Initializes the two {@code RectF} objects {@link BatteryProgressView#progress},
     * {@link BatteryProgressView#progressBackground} and sets the stroke width of
     * {@link BatteryProgressView#whiteLine}
     */
    void initVars(){
        progress = new RectF(wp2p(0,0), hp2p(0,0), wp2p(100,0), hp2p(100,0));
        progressBackground = new RectF(wp2p(0,0), hp2p(0,0), wp2p(100,0), hp2p(100,0));
        whiteLine.setStrokeWidth(wp2p(0,5));
    }

    /**
     * Draws custom progress bar using
     * {@link BatteryProgressView#drawCustomProgressbar(int)} and draws the
     * {@link BatteryProgressView#whiteLine}
     *
     * @param canvas drawing area
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        drawCustomProgressbar(batteryState);
        canvas.drawRect(progressBackground, progressBackgroundPaint); //background
        canvas.drawRect(progress, progressPaint); //progress bar
        canvas.drawLine(wp2p(batteryState,0), hp2p(0,0),
                wp2p(batteryState,0), hp2p(100,0), whiteLine); //whit progress bar tip line
    }

    /**
     * Gets the battery percentage, sets it as {@link BatteryProgressView#batteryState} and
     * invalidates the {@code View}.
     *
     * @param batteryLevel battery percentage to be set
     */
    public void setBatteryLevel(int batteryLevel){
        this.batteryState = batteryLevel;
        invalidate();
    }

    /**
     * Draws progress bar according to {@link BatteryProgressView#batteryState}. <br>
     * If {@link BatteryProgressView#batteryState} is between:
     * <ul>
     *     <li><b>0 and 10:</b> the progress bar is drawn in red</B></li>
     *     <li><b>11 and 20:</b> the progress bar is drawn in orange</li>
     *     <li><b>21 and 100:</b> the progress bar is drawn in green</li>
     * </ul>
     *
     * @param batteryState battery percentage to be drawn
     */
    private void drawCustomProgressbar(int batteryState){
        if (batteryState <= 20 && batteryState > 10){
            progressPaint.setColor(Color.rgb(255,136,0));
            progressBackgroundPaint.setColor(Color.argb(120,255,136,0));
        }
        else if (batteryState <= 10){
            progressPaint.setColor(Color.RED);
            progressBackgroundPaint.setColor(Color.argb(120,255,0,0));
        }
        else {
            progressPaint.setColor(Color.rgb(0,255,0));
            progressBackgroundPaint.setColor(Color.argb(120,0,255,0));
        }

        progress.right = wp2p(batteryState,0);
    }

    /**
     * @see LegStatusView#wp2p(float, float)
     */
    //takes a percentage of View-width and converts it to pixels
    private float wp2p(float widthPercentage, float offsetDpi){
        float offPx;
        if (offsetDpi != 0 ) offPx = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, offsetDpi, displayMetrics);
        else offPx = 0.0f;
        return ((widthPercentage/100)*width + offPx);
    }

    /**
     * @see LegStatusView#hp2p(float, float)
     */
    //takes a percentage of View-height and converts it to pixels
    private float hp2p(float heightPercentage, float offsetDpi){
        float offPx;
        if (offsetDpi != 0 ) offPx = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, offsetDpi, displayMetrics);
        else offPx = 0.0f;
        return ((heightPercentage/100)*height + offPx);
    }

}


