/******************************************************************************
 * Copyright (c) 2017 All Rights Reserved, http://www.rsl.ethz.ch             *
 * ETH Zurich                                                                 *
 * Robotic Systems Lab                                                        *
 * Leonhardstrasse 21, LEE J201                                               *
 * 8092 Zurich                                                                *
 * Switzerland                                                                *
 * ---                                                                        *
 * Contributors:                                                              *
 * - Flurin Schwerzmann (f.schwerzmann@gmail.com)                             *
 * - Luca Vandeventer (looc.vdv@hotmail.com)                                  *
 * - Samuel Bachmann (samuel.bachmann@gmail.com)                              *
 ******************************************************************************/

package com.ATHLAS.operator_terminal.ui;

import android.app.ActivityManager;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

import com.ATHLAS.operator_terminal.R;
import com.ATHLAS.operator_terminal.ros.nodes.CustomNode;
import com.ATHLAS.operator_terminal.ros.nodes.OperatorTerminalNode;

import org.ros.node.NodeConfiguration;
import org.ros.node.NodeMainExecutor;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Provides a framework for the various Fragments including drawer operation and leg height/leg force
 * /earth limits as well as an {@link OperatorTerminalNode} instance.
 */
public class MainActivity extends AppCompatActivity
    implements NavigationView.OnNavigationItemSelectedListener {

  private static final String TAG = "MainActivity";

  /**
   * Needed in {@link #onActivityResult(int, int, Intent)} for correct handling of return from settings.
   */
  private static final int SETTINGS_RESULT = 1;

  @BindView(R.id.toolbar) Toolbar toolbar;
  @BindView(R.id.drawerLayout) DrawerLayout drawer;
  @BindView(R.id.navigationView) public NavigationView navigationView;
  @BindString(R.string.KEY_LLC_ACTIVE_CONTROLLER) String keyLlcActiveController_;
  @BindString(R.string.KEY_LLC_ACTIVE_MODE) String keyLlcActiveMode_;

  private boolean isCreatedNew = true;
  public MainApplication mainApplication;
  private NodeMainExecutor nodeMainExecutor;
  private PowerManager.WakeLock wakeLock_ = null;
  private WifiManager.WifiLock wifiLock_ = null;
  private FragmentManager fragmentManager;
  protected NodeConfiguration nodeConfiguration;

  /**
   * The currently selected UI, needed for correct reconfiguration of UI elements and ROS framework.
   */
  static int activeMainDisplay = R.id.nav_ros_master;

  public BatteryFragment batteryFragment;
  public TextStatusFragment textStatusFragment;
  public TestFragment testFragment;
  public LegDisplayFragment legDisplayFragment;
  public ROSMasterFragment serverFragment;
  public ControlLeverFragment controlFragment;

  Resources resources;
  SharedPreferences sharedPreferences;

  public float legForceUpperLimit,
            legForceWarning,
            legForceLowerLimit;
  public float legLowerLimitF,
          legUpperLimitF,
          earthUpperLimitF,
          earthLowerLimitF;
  public String legLowerLimitS,
          legUpperLimitS,
          earthUpperLimitS,
          earthLowerLimitS;
  public boolean forceLimitsReady = false;

  /**
   * Connected ROS framework.
   */
  OperatorTerminalNode operatorTerminalNode;

  /**
   * Initialize variables, configure environment and fragments, set up drawer and wifi lock,
     */
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.activity_main);
    ButterKnife.bind(this);

    initFragments();

    fragmentManager = getFragmentManager();

    // Set keep screen on flag.
    getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

    // Set toolbar.
    setSupportActionBar(toolbar);

    //Drawer set up
    ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(
        this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
    drawer.addDrawerListener(actionBarDrawerToggle);
    actionBarDrawerToggle.syncState();
    navigationView.setNavigationItemSelectedListener(this);

    // Change the bar color for the Android overview view. This works from API 21 (Lollipop).
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      setTaskDescription(new ActivityManager.TaskDescription(
          getResources().getString(R.string.app_name),
          BitmapFactory.decodeResource(getResources(), R.drawable.rsl_icon), Color.WHITE));
    }

    // Initialize main application
    mainApplication = (MainApplication) getApplicationContext();

    //first start stuff
    if (savedInstanceState == null) {
      Log.i(TAG, "is created new!");
      isCreatedNew = true;
    } else {
      Log.i(TAG, "is not new!");
      isCreatedNew = false;
    }

    // set immersive view in onCreate
    setupMainWindowDisplayMode();

    // Set wake and wifi lock.
    PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
    wakeLock_ = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
    wakeLock_.acquire();
    int wifiLockType = WifiManager.WIFI_MODE_FULL;
    try {
      wifiLockType = WifiManager.class.getField("WIFI_MODE_FULL_HIGH_PERF").getInt(null);
    } catch (Exception e) {
      // We must be running on a pre-Honeycomb device.
      Log.w(TAG, "Unable to acquire high performance wifi lock.");
    }
    WifiManager wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
    wifiLock_ = wifiManager.createWifiLock(wifiLockType, TAG);
    wifiLock_.acquire();

    // Initialize SharedPreference and Resources access and reset active controller/mode
    sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    SharedPreferences.Editor editor = sharedPreferences.edit();
    editor.putString(keyLlcActiveController_, "");
    editor.putString(keyLlcActiveMode_, "");
    editor.apply();
    resources = getResources();

    //load limits
    forceLimitsReady = loadForceLimits();
    loadLegLimits();
    loadEarthLimits();
  }

  @Override
  public void onStart() {
    super.onStart();

    if (isCreatedNew) {
      runOnUiThread(new Runnable() {
        @Override
        public void run() {
          fragmentManager.beginTransaction().replace(R.id.left_main_frame, serverFragment).commit();
          navigationView.setCheckedItem(R.id.nav_ros_master);
        }
      });
    }

    Log.i(TAG, "called onStart");
  }

  /**
   * Keeps immersive {@code View} after {@link SplashActivity#onResume()} is called using
   * {@link #setupMainWindowDisplayMode()}.
   */
  @Override
  public void onResume() {
    super.onResume();
    Log.i(TAG, "called onResume()");

    isCreatedNew = false;

    setupMainWindowDisplayMode();
  }

  /**
   * Sets the {@code View} immersive using
   * {@link #setSystemUiVisiblityMode()}. Screen will appear in fullscreen mode.
   */
  private void setupMainWindowDisplayMode() {
    View decorView = setSystemUiVisiblityMode();

    decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
      @Override
      public void onSystemUiVisibilityChange(int visibility) {
        setSystemUiVisiblityMode(); // Needed to avoid exiting immersive_sticky when keyboard is displayed
        if((visibility & View.SYSTEM_UI_FLAG_HIDE_NAVIGATION) == 0){
          getSupportActionBar().show();
        } else {
          getSupportActionBar().hide();
        }
      }
    });
  }

  /**
   * Sets the {@code View} immersive. This means the View is fullscreen and
   * the navigation bar and the status bars are hidden. If the user swipes from the bottom of the
   * screen upwards, both appear in transparent.
   *
   * @return top-level window decor view
   */
  private View setSystemUiVisiblityMode() {
    View decorView = getWindow().getDecorView();
    int options;
    options =
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE // makes display content appear under navigaion bar when swiped down -> Screen doesn't resize
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                    | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
    try {
      getSupportActionBar().hide();
    } catch (NullPointerException e) {
      Log.e(TAG, e.toString());
    }

    decorView.setSystemUiVisibility(options);
    return decorView;
  }

  /**
   * Get force limits as {@code String} from {@link SharedPreferences} and parse to
   * {@code legForceLowerLimit}, {@code legForceWarning} and {@code legForceUpperLimit}. <br>
   *
   * An error "FORCE LIM NOT AVAIL" is pushed, if parsing fails.
   *
   * @return true, if limits were loaded correctly <br>
   *     false, otherwise.
   */
  private boolean loadForceLimits(){
    try {
      legForceLowerLimit = Float.parseFloat(sharedPreferences.getString(resources.getString(R.string.KEY_FORCE_LIMITS_LOWER), "0"));
      legForceWarning = Float.parseFloat(sharedPreferences.getString(resources.getString(R.string.KEY_FORCE_LIMITS_WARNING), "0"));
      legForceUpperLimit = Float.parseFloat(sharedPreferences.getString(resources.getString(R.string.KEY_FORCE_LIMITS_UPPER), "0"));

      return true;
    } catch (NumberFormatException e){
      Log.e(TAG, e.toString());
      textStatusFragment.pushError("FORCE LIM NOT AVAIL");
      return false;
    }
  }

  /**
   * Get leg movement limits as {@code String} from {@link SharedPreferences} and parse to
   * {@code legLowerLimitF} and {@code legUpperLimitF}. The readable Strings {@code legLowerLimitS}
   * and {@code legUpperLimitS} are updated, too. <br>
   * An error "LEG LIM NOT AVAIL" is pushed, if parsing fails.
   *
   * @return true, if limits were loaded correctly <br>
   *     false, otherwise
   */
  private boolean loadLegLimits(){
    try {
      legLowerLimitF = Float.parseFloat(sharedPreferences.getString(resources.getString(R.string.KEY_LEG_LIMITS_LOWER), "0"));
      legUpperLimitF = Float.parseFloat(sharedPreferences.getString(resources.getString(R.string.KEY_LEG_LIMITS_UPPER), "0"));
      legLowerLimitS = "Lower leg limit: " + legLowerLimitF;
      legUpperLimitS = "Upper leg limit: " + legUpperLimitF;

      return true;
    } catch (NumberFormatException e){
      legLowerLimitS = "Lower leg limit: N/A";
      legUpperLimitS = "Upper leg limit: N/A";

      textStatusFragment.pushError("LEG LIM NOT AVAIL");
      return false;
    }
  }

  /**
   * Get leg movement limits as {@code String} from {@link SharedPreferences} and parse to
   * {@code earthLowerLimitF} and {@code earthUpperLimitF}. The readable Strings {@code earthLowerLimitS}
   * and {@code earthUpperLimitS} are updated, too. <br>
   * An error "EARTH LIM NOT AVAIL" is pushed, if parsing fails.
   *
   * @return true, if limits were loaded correctly <br>
   *     false, otherwise
   */
  private boolean loadEarthLimits(){
    try {
      earthUpperLimitF = Float.parseFloat(sharedPreferences.getString(resources.getString(R.string.KEY_EARTH_LIMITS_UPPER), "0"));
      earthLowerLimitF = Float.parseFloat(sharedPreferences.getString(resources.getString(R.string.KEY_EARTH_LIMITS_LOWER), "0"));
      earthUpperLimitS = "Upper earth limit: " + earthUpperLimitF;
      earthLowerLimitS = "Lower earth limit: " + earthLowerLimitF;

      return true;
    } catch (NumberFormatException e){
      earthUpperLimitS = "Upper earth limit: N/A";
      earthLowerLimitS = "Lower earth limit: N/A";

      textStatusFragment.pushError("EARHT LIM NOT AVAIL");
      return false;
    }
  }

  /**
   * Initializes all the {@code Fragment} objects.
   *
   * @see BatteryFragment
   * @see TextStatusFragment
   * @see TestFragment
   * @see LegDisplayFragment
   * @see ROSMasterFragment
   * @see ControlLeverFragment
   */
  private void initFragments(){
    batteryFragment = new BatteryFragment();
    textStatusFragment = new TextStatusFragment();
    testFragment = new TestFragment();
    legDisplayFragment = new LegDisplayFragment();
    serverFragment = new ROSMasterFragment();
    controlFragment = new ControlLeverFragment();
  }

  /**
   * Starts the {@link OperatorTerminalNode}. Called, when either the maintenance or operator
   * terminal UI are selected.
   */
  private void startOperatorTerminalNode(){
    if(operatorTerminalNode == null) {
      operatorTerminalNode = new OperatorTerminalNode("ATHLAS_operator_terminal", this);
      executeNode(operatorTerminalNode);
    }
  }

  /**
   * Launches a ROS node. Currently only used locally to start the {@link OperatorTerminalNode}.
   *
   * @param customNode The rosjava-node to be started.
     */
  private void executeNode(CustomNode customNode) {
    nodeMainExecutor = mainApplication.getNodeMainExecutor();
    nodeConfiguration = mainApplication.getNodeConfiguration();
    if (nodeMainExecutor != null) {
      nodeMainExecutor.execute(customNode, nodeConfiguration);
    } else{
      Log.d(TAG, "nodeMainExecutor is null");
    }
  }

  /**
   * Opens the {@link UserSettingActivity settings} activity.
   */
  private void openSettingsActivity() {
    Intent intent = new Intent(MainActivity.this, UserSettingActivity.class);
    startActivityForResult(intent, SETTINGS_RESULT);
  }

  /**
   * Reloads leg, earth and force limits when returning from settings activity.
   */
  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    //reload limits
    if(requestCode == SETTINGS_RESULT) {
      loadLegLimits();
      loadEarthLimits();
      loadForceLimits();
    }

    navigationView.setCheckedItem(activeMainDisplay);
  }

  /**
   * Switches drawer action and subsequently manages the Fragments.
     */
  @Override
  public boolean onNavigationItemSelected(MenuItem item) {
    switch (item.getItemId()){
      case R.id.nav_ros_master:
          fragmentManager.beginTransaction().replace(R.id.left_main_frame, serverFragment)
                  .remove(batteryFragment)
                  .remove(legDisplayFragment)
                  .remove(textStatusFragment)
                  .remove(testFragment)
                  .remove(controlFragment)
                  .commit();
          activeMainDisplay = R.id.nav_ros_master;
        break;
      case R.id.nav_maintenance_terminal:
        startOperatorTerminalNode();
        fragmentManager.beginTransaction().replace(R.id.right_lower_frame, textStatusFragment)
                .replace(R.id.left_main_frame, legDisplayFragment)
                .replace(R.id.right_upper_frame, batteryFragment)
                .replace(R.id.right_center_frame, testFragment)
                .commit();
        activeMainDisplay = R.id.nav_maintenance_terminal;
        break;
      case R.id.nav_operator_terminal:
        startOperatorTerminalNode();
        fragmentManager.beginTransaction().replace(R.id.right_lower_frame, textStatusFragment)
                .replace(R.id.left_main_frame, legDisplayFragment)
                .replace(R.id.right_upper_frame, batteryFragment)
                .replace(R.id.right_center_frame, controlFragment)
                .commit();
        activeMainDisplay = R.id.nav_operator_terminal;
        break;
      case R.id.action_settings:
        openSettingsActivity();
        break;
    }

    // Close navigation drawer.
    drawer.closeDrawer(GravityCompat.START);

    return true;
  }

  /**
   * Shuts down ROS.
   */
  @Override
  public void onDestroy() {
    super.onDestroy();

    mainApplication = (MainApplication) getApplicationContext();

    //copied from RosNodeFragment
    if (mainApplication != null) {
      nodeMainExecutor = mainApplication.getNodeMainExecutor();
      if (nodeMainExecutor != null) {
        nodeMainExecutor.shutdown();
        mainApplication.setNodeMainExecutor(nodeMainExecutor);
      }
    }

    // release wake lock
    if (wakeLock_.isHeld()) {
      wakeLock_.release();
    }
    // release wifi lock
    if (wifiLock_.isHeld()) {
      wifiLock_.release();
    }

  }
}
