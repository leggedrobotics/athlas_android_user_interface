package com.ATHLAS.operator_terminal.ui;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.util.Log;

import com.ATHLAS.operator_terminal.R;

// The main preference nextFragment class
public class MyPreferenceFragment extends PreferenceFragment
    implements Preference.OnPreferenceClickListener, SharedPreferences.OnSharedPreferenceChangeListener {

  private Callback mCallback;

  private static final String KEY_1 = "KEY_COMPETITION_SELECTION";
  private static final String KEY_2 = "NESTED_KEY2";

  private Activity myActivity;

  EditTextPreference IPETPref,
          portETPref,
          lowerLegLimitETPref,
          upperLegLimitETPref,
          upperEarthLimitETPref,
          lowerEarthLimitETPref,
          lowerForceLimitETPref,
          warningForceLimitETPref,
          upperForceLimitETPref,
          maxBatteryETPref,
          minBatteryETPref;

  Preference resetLegHeightPref, resetEarthHeightPref, resetForceLimitsPref, resetBatteryLimitsPref;
  SharedPreferences sharedPreferences;

  @Override
  public void onAttach(Activity activity) {
    super.onAttach(activity);

    myActivity = activity;

    if (activity instanceof Callback) {
      mCallback = (Callback) activity;
    } else {
      throw new IllegalStateException("Owner must implement Callback interface");
    }
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    addPreferencesFromResource(R.xml.preferences);

    sharedPreferences = getPreferenceManager().getSharedPreferences();
    sharedPreferences.registerOnSharedPreferenceChangeListener(this);

    resetLegHeightPref = findPreference(getString(R.string.KEY_LEG_LIMITS_RESET));
    resetLegHeightPref.setOnPreferenceClickListener(this);

    resetEarthHeightPref = findPreference(getString(R.string.KEY_EARTH_LIMITS_RESET));
    resetEarthHeightPref.setOnPreferenceClickListener(this);

    resetForceLimitsPref = findPreference(getString(R.string.KEY_FORCE_LIMITS_RESET));
    resetForceLimitsPref.setOnPreferenceClickListener(this);

    resetBatteryLimitsPref = findPreference(getString(R.string.KEY_BAT_RESET));
    resetBatteryLimitsPref.setOnPreferenceClickListener(this);

    IPETPref = (EditTextPreference) findPreference(getString(R.string.KEY_ROS_MASTER_IP));
    portETPref = (EditTextPreference) findPreference(getString(R.string.KEY_ROS_MASTER_PORT));

    lowerLegLimitETPref = (EditTextPreference) findPreference(getString(R.string.KEY_LEG_LIMITS_LOWER));
    upperLegLimitETPref = (EditTextPreference) findPreference(getString(R.string.KEY_LEG_LIMITS_UPPER));

    upperEarthLimitETPref = (EditTextPreference) findPreference(getString(R.string.KEY_EARTH_LIMITS_UPPER));
    lowerEarthLimitETPref = (EditTextPreference) findPreference(getString(R.string.KEY_EARTH_LIMITS_LOWER));

    lowerForceLimitETPref = (EditTextPreference) findPreference(getString(R.string.KEY_FORCE_LIMITS_LOWER));
    warningForceLimitETPref = (EditTextPreference) findPreference(getString(R.string.KEY_FORCE_LIMITS_WARNING));
    upperForceLimitETPref = (EditTextPreference) findPreference(getString(R.string.KEY_FORCE_LIMITS_UPPER));

    minBatteryETPref = (EditTextPreference) findPreference(getString(R.string.KEY_BAT_MIN));
    maxBatteryETPref = (EditTextPreference) findPreference(getString(R.string.KEY_BAT_MAX));
  }

  @Override
  public void onResume() {
    super.onResume();
    updateSummaries();
  }

  @Override
  public void onPause() {
    getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    super.onPause();
  }

  @Override
  public boolean onPreferenceClick(Preference preference) {
    Log.d("MyPrefFrag", "pref key: " + preference.getKey());

    if(preference.getKey().equals(getString(R.string.KEY_LEG_LIMITS_RESET))){
      sharedPreferences.edit().putString(getString(R.string.KEY_LEG_LIMITS_UPPER), getString(R.string.preferences_leg_limits_default_upper)).apply();
      sharedPreferences.edit().putString(getString(R.string.KEY_LEG_LIMITS_LOWER), getString(R.string.preferences_leg_limits_default_lower)).apply();

      return true;
    } else

    if(preference.getKey().equals(getString(R.string.KEY_EARTH_LIMITS_RESET))){
      sharedPreferences.edit().putString(getString(R.string.KEY_EARTH_LIMITS_LOWER), getString(R.string.preferences_earth_limits_default_lower)).apply();
      sharedPreferences.edit().putString(getString(R.string.KEY_EARTH_LIMITS_UPPER), getString(R.string.preferences_earth_limits_default_upper)).apply();

      return true;
    } else

    if(preference.getKey().equals(getString(R.string.KEY_FORCE_LIMITS_RESET))){
      sharedPreferences.edit().putString(getString(R.string.KEY_FORCE_LIMITS_LOWER), getString(R.string.preferences_force_limits_default_lower)).apply();
      sharedPreferences.edit().putString(getString(R.string.KEY_FORCE_LIMITS_WARNING), getString(R.string.preferences_force_limits_default_warning)).apply();
      sharedPreferences.edit().putString(getString(R.string.KEY_FORCE_LIMITS_UPPER), getString(R.string.preferences_force_limits_default_upper)).apply();

      return true;
    } else

    if(preference.getKey().equals(getString(R.string.KEY_BAT_RESET))){
      sharedPreferences.edit().putString(getString(R.string.KEY_BAT_MAX), getString(R.string.preferences_battery_default_max)).apply();
      sharedPreferences.edit().putString(getString(R.string.KEY_BAT_MIN), getString(R.string.preferences_battery_default_min)).apply();

      return true;
    } else

    return false;
  }

  @Override
  public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
    updateSummaries();
  }

  public interface Callback {
    void onNestedPreferenceSelected(int key);
  }

  private void updateSummaries(){
    IPETPref.setSummary(sharedPreferences.getString(getString(R.string.KEY_ROS_MASTER_IP), "N/A"));
    portETPref.setSummary(sharedPreferences.getString(getString(R.string.KEY_ROS_MASTER_PORT), "N/A"));

    lowerLegLimitETPref.setSummary(sharedPreferences.getString(getString(R.string.KEY_LEG_LIMITS_LOWER), "N/A"));
    upperLegLimitETPref.setSummary(sharedPreferences.getString(getString(R.string.KEY_LEG_LIMITS_UPPER), "N/A"));

    upperEarthLimitETPref.setSummary(sharedPreferences.getString(getString(R.string.KEY_EARTH_LIMITS_UPPER), "N/A"));
    lowerEarthLimitETPref.setSummary(sharedPreferences.getString(getString(R.string.KEY_EARTH_LIMITS_LOWER), "N/A"));

    lowerForceLimitETPref.setSummary(sharedPreferences.getString(getString(R.string.KEY_FORCE_LIMITS_LOWER), "N/A"));
    warningForceLimitETPref.setSummary(sharedPreferences.getString(getString(R.string.KEY_FORCE_LIMITS_WARNING), "N/A"));
    upperForceLimitETPref.setSummary(sharedPreferences.getString(getString(R.string.KEY_FORCE_LIMITS_UPPER), "N/A"));

    maxBatteryETPref.setSummary(sharedPreferences.getString(getString(R.string.KEY_BAT_MAX), "N/A"));
    minBatteryETPref.setSummary(sharedPreferences.getString(getString(R.string.KEY_BAT_MIN), "N/A"));
  }
}