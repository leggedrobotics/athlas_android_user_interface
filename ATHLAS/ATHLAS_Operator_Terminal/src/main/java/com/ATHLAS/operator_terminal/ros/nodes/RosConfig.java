package com.ATHLAS.operator_terminal.ros.nodes;

/**
 * Storage for ROS URIs (Services and Topics);
 */
interface RosConfig {

  //Services
  String GET_LEG_HEIGHT_SERVICE = "/getLegHeight";
  String SET_LEG_HEIGHT_SERVICE = "/setLegHeight";
  String OT_BRAKE_STATE_SERVICE = "/OTBrakeStateSrv";
  String SET_BRAKE_STATE_SERVICE = "/setBrakeState";
  String INIT_FLYING_MODE_SERVICE = "/initFlying";
  String INIT_LANDING_MODE_SERVICE = "/initLanding";
  String SEND_EMERGENCY_SERVICE = "/emergency";

  //Topics
  String SET_LEG_HEIGHT_TOPIC = "/setLegHeight";
  String UPDATE_LEG_HEIGHT_TOPIC = "/updateLegHeight";
  String UPDATE_LEG_FORCE_TOPIC = "/updateLegForce";
  String UPDATE_EARTH_HEIGHT_TOPIC = "/updateEarthHeight";
  String UPDATE_BATTERY_VOLTAGE = "/updateBatteryVoltage";
  String OT_BRAKE_STATE_TOPIC = "/OTBrakeStateTopic";
}
