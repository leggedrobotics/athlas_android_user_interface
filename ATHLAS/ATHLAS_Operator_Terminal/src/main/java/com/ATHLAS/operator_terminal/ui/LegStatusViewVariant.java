package com.ATHLAS.operator_terminal.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.util.AttributeSet;

import com.ATHLAS.operator_terminal.R;

/**
 * First variant of a leg display. Styling is more abstract and asymmetric. Makes use of a custom
 * XML attribute ({@code custom:isRightSide="true/false"}).
 *
 * @author Flurin Schwerzmann
 */

public class LegStatusViewVariant extends LegStatusView {

    /**
     * Used to adjust view styling depending on the position of the corresponding leg.
     * Set using the custom XML attribute {@code custom:isRightSide="true/false"}.
     */
    boolean isRightSide;

    public LegStatusViewVariant(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray attrs = context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.LegStatusViewVariant, 0, 0);
        isRightSide = attrs.getBoolean(R.styleable.LegStatusViewVariant_isRightSide, false);
    }

    /**
     * Draw an abstract leg with a foot line. {@link #isRightSide Asymmetry} is incorporated.
     *
     * @param canvas drawing area
     */
    @Override
    void drawLeg(Canvas canvas) {
        //leg
        if(isRightSide) canvas.drawRect(wp2p(0, 0), hp2p(18, 5), wp2p(60, 0), hp2p(legViewPercentage, 0), legPaint);
        else canvas.drawRect(wp2p(40, 0), hp2p(18, 5), width, hp2p(legViewPercentage, 0), legPaint);

        //foot line
        if(isRightSide) canvas.drawLine(wp2p(0, 0), hp2p(legViewPercentage, 0), wp2p(60, 0), hp2p(legViewPercentage, 0), footPaint);
        else canvas.drawLine(wp2p(40, 0), hp2p(legViewPercentage, 0), width, hp2p(legViewPercentage, 0), footPaint);
    }

    /**
     * {@code footPaint} has doubled width.
     */
    @Override
    void initPaint() {
        super.initPaint();
        footPaint.setStrokeWidth(2*broaderStrokeWidth);
    }

    /**
     * The leg state frame is drawn on the view's borders, there is no purple view borders frame.
     * Draws a white separator line at the bottom of the leg height percentage text.
     *
     * @param canvas drawing area
     */
    @Override
    void drawFrame(Canvas canvas) {
        //leg height percentage (text)
        canvas.drawText(String.valueOf((int) legHeightPercentage) + "%", wp2p(50, 0), hp2p(10, 0) - (textPaint.descent() + textPaint.ascent())/2, textPaint);

        //frame rectangle
        canvas.drawRect(0, 0, width, height, legStatePaint); //leg state dependent colored frame

        //leg travel lines
        float horizonLineMargin = 5;
        canvas.drawLine(wp2p(0, horizonLineMargin), hp2p(LEG_MAX_LENGTH_HEIGHT_PERCENTAGE, 0), wp2p(100, -horizonLineMargin),
                hp2p(LEG_MAX_LENGTH_HEIGHT_PERCENTAGE, 0),
                maxLegLengthPaint);
        canvas.drawLine(wp2p(0, horizonLineMargin), hp2p(18, 5), wp2p(100, -horizonLineMargin), hp2p(18, 5), maxLegLengthPaint);
    }

    /**
     * Massively simpler drawn foot base - a sober grey rectangle on the right/left.
     *
     * @param canvas drawing area
     */
    @Override
    void drawFootBase(Canvas canvas) {
        if(isRightSide) canvas.drawRect(wp2p(60, 0), hp2p(18, 5), wp2p(100, 0), hp2p(LEG_MIN_LENGTH_HEIGHT_PERCENTAGE, 0)+broaderStrokeWidth, footBasePaint);
        else canvas.drawRect(wp2p(0, 0), hp2p(18, 5), wp2p(40, 0), hp2p(LEG_MIN_LENGTH_HEIGHT_PERCENTAGE, 0)+broaderStrokeWidth, footBasePaint);
    }

}
