package com.ATHLAS.operator_terminal.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Displays the RSL logo for a short time during startup and afterwards continues with the
 * {@link MainActivity}.
 *
 * @author Samuel Bachmann
 */
public class SplashActivity extends AppCompatActivity {
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    Intent intent = new Intent(this, MainActivity.class);
    startActivity(intent);
    finish();
  }
}
