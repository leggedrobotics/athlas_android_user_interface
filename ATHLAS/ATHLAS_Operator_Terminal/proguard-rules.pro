# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in C:\Users\Samuel\AppData\Local\Android\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-dontoptimize
#-dontshrink
#-dontusemixedcaseclassnames
#-dontskipnonpubliclibraryclasses
#-dontpreverify
#-verbose

-keepattributes EnclosingMethod
-keepattributes InnerClasses

# Retain generated class which implement Unbinder.
-keep public class * implements butterknife.Unbinder { public <init>(...); }

# Prevent obfuscation of types which use ButterKnife annotations since the simple name
# is used to reflectively look up the generated ViewBinding.
-keep class butterknife.*
-keepclasseswithmembernames class * { @butterknife.* <methods>; }
-keepclasseswithmembernames class * { @butterknife.* <fields>; }

#-keep class org.joda.time.** { *; }

#-dontobfuscate
#-optimizationpasses 5
#-dontusemixedcaseclassnames
#-dontskipnonpubliclibraryclasses
#-dontpreverify
#-verbose
#-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*,!code/allocation/variable

#-dontwarn org.jboss.netty.**
#-dontwarn org.ros.android.**
#-dontwarn org.ros.gradle_plugins.**
#-dontwarn org.ros.internal.system.**
#-dontwarn org.apache.commons.**
#-dontwarn org.apache.xmlrpc.**
#-dontwarn com.google.common.**
#-dontwarn org.xbill.DNS.**

#-keep class org.apache.xmlrpc.** {
#    *;
#}

#-keep class org.apache.commons.** {
#    *;
#}

#-keep class org.apache.http.** {
#    *;
#}

#-keep class cz.msebera.android.httpclient.** {
#    *;
#}

#-keep class com.loopj.android.http.** {
#    *;
#}

#-keep class com.squareup.okhttp.** {
#    *;
#}

#-keep class org.jsoup.** {
#    *;
#}

#-keep class org.joda.time.** {
#    *;
#}

#-keep public class com.google.android.gms.* { public *; }
#-dontwarn com.google.android.gms.**

#-dontwarn java.nio.file.*
#-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement

## Support library v4 22.2.0
#-dontwarn android.support.v4.**
#-dontwarn android.support.v7.media.**
#-dontwarn com.androidquery.auth.TwitterHandle*

#-libraryjars libs/joda-time/joda-time-2.7.jar
#-libraryjars libs/joda-convert/joda-convert-1.7.jar

#-assumenosideeffects class android.util.Log {
#    public static *** d(...);
#    public static *** w(...);
#    public static *** v(...);
#    public static *** i(...);
#}

## don't warn beta classes ?!?
#-dontwarn ch.saduino.apps.wapsrf.*

#-assumenosideeffects class android.widget.Toast {
#    public static *** makeText(...);
#    public void show();
#}