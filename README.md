# ATHLAS Operator Terminal

Android app for intuitive control of the ATHLAS system. It is designed to be used with a 10" tablet. 

Bachelor thesis of Flurin Schwerzmann and Luca Vandeventer, spring 2017.

**Abstract**
The All-Terrain Helicopter Landing System (ATHLAS) overcomes major limitations of conventional landing gears by making possible landings on steep and uneven surfaces. 
The Operator Terminal augments the unmanned ATHLAS prototype by improving accessibility. It adheres to aviation standards with regard to safety, reliability and ease 
of use besides connecting wirelessly.

Development of the Operator Terminal was conducted iteratively with continuous testing and refinement. The User Interface (UI) was structured to allow
for flexible reconfiguration as well as concurrent development. Custom UI elements for the unique legged landing gear configuration of ATHLAS were modeled
with existing avionic displays in mind. The Operator Terminal is realized on an Android tablet using Java, the Robot Operating System (ROS) and a WiFi connection.  

The resulting application provides a mature UI for ATHLAS. It proved its operational fitness during several public demonstrations. Implemented features include 
a visualization of the system’s state and control capabilities for the pilot as well as a maintenance engineer. Finally, a Swiss Air Force helicopter pilot
verified our work to be close to reality. Nonetheless, the Operator Terminal offers potential for further development, such as integration of additional sensors 
on ATHLAS or upscaling to a manned helicopter.

**Development Environment**
PC: Ubuntu 14.04 LTS (32-bit) running on an Acer Aspire 1830T. 
Programming Suite: Android Studio 2.2.3 with Gradle plug-in 2.2.3, android-apt 1.8, rosjava 0.2.2 and ROS Indigo. 
Tablet: Android 7.1.2 running on a Google Pixel C

**Landed Aeroscout UAV with mounted ATHLAS**
![ATHLASvorHG.jpg](https://bitbucket.org/repo/p4Kbd7z/images/3804350617-ATHLASvorHG.jpg)

**Operator Terminal in action**
![titlepage_picture_small.jpg](https://bitbucket.org/repo/p4Kbd7z/images/1578383905-titlepage_picture_small.jpg)

**Screenshot of Operator Terminal**
![landed.png](https://bitbucket.org/repo/p4Kbd7z/images/1395884598-landed.png)